<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/all.css',
        'css/fontawesome-iconpicker.min.css',
    ];
    public $js = [
        'js/all.js',
        'js/rowsorter.js',
        'js/fontawesome-iconpicker.min.js',
		'js/highcharts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
