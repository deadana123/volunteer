<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box box-info">
    <div class="box-body">

		<p>
			<?= Html::a("<i class='fa fa-arrow-left'></i> Kembali", ['index'], ['class' => 'btn btn-default']) ?>
		</p>
		<hr/>
		<div class="col-md-12" style="display:flex;justify-content:center;align-items:center;">
			<img id="foto" style="width:50%;height:300px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>">
		</div>
		<div class="col-md-12">
			<h2>Detail</h2>
			<hr/>
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'nama:ntext',
					[
						'label'  => 'Deskripsi',
						'value'  => $model->isi,
						'captionOptions' => ['style' => 'width:200px'],
					],
					[
						'label'  => 'Kategori',
						'value'  => $model->newsKategori->nama
					],
					'view',
				],
			]) ?>
		</div>
	</div>
</div>
