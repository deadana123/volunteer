<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\EventVolunteer;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<div class="box box-info">
    <div class="box-body">
        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>

        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'],
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
            'headerRowOptions' => ['class' => 'x'],
            'columns' => [

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} {view}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-eye'></i>", ["view", "id" => $model->id], ["class" => "btn btn-success", "title" => "Lihat Data"]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-pen'></i>", ["update", "id" => $model->id], ["class" => "btn btn-warning", "title" => "Edit Data"]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-trash'></i>", ["delete", "id" => $model->id], [
                                "class" => "btn btn-danger",
                                "title" => "Hapus Data",
                                "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                                "data-method" => "POST"
                            ]);
                        },
                        'list-volunteer' => function ($url, $model, $key){
							$countVolunteer = EventVolunteer::find()->where(['event_id'=>$model->id,'status'=>0])->count();
                            if($countVolunteer>0){								
								return Html::a("<i class='fa fa-list'></i> <span class='badge badge-light'>".$countVolunteer."</span>", ["detail", "id" => $model->id], ["class" => "btn btn-info", "title" => "Detail"]);
							}else{
								return Html::a("<i class='fa fa-list'></i>", ["detail", "id" => $model->id], ["class" => "btn btn-info", "title" => "Detail"]);
							}
                        },
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:left;width:140px']
                ],
                'nama',
                'kuota',
				[
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'status',
					'label'=>'Status',
                    'value' => function ($model) {
						if($model->status==0){								
							return Html::button("Menunggu Konfirmasi",["class" => "btn btn-default btn-change-aktif", "title" => "menunggu"]);
						}else if($model->status==1){								
							return Html::button("Acara Diterima",["class" => "btn btn-success btn-change-nonaktif", "title" => "diterima"]);
						}else{
							return Html::button("Acara Ditolak",["class" => "btn btn-danger btn-change-nonaktif", "title" => "ditolak"]);
						}
                    },
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
                    'format' => 'raw',
                ],
				[
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'Action',
					'label'=>'action',
                    'value' => function ($model) {
						if($model->status==0){								
							return Html::a("<i class='fa fa-check'></i>",['terima-event','id'=>$model->id],["class" => "btn btn-success btn-change-aktif", "title" => "Tidak aktif"]).' '.Html::a("<i class='fa fa-times'></i>",['tolak-event','id'=>$model->id],["class" => "btn btn-danger btn-change-aktif", "title" => "Tidak aktif"]);
						}else{
							return '';
						}
                    },
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
                    'format' => 'raw',
                ],
				[
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => '#',
					'label'=>'#',
                    'value' => function ($model) {
						$countVolunteer = EventVolunteer::find()->where(['event_id'=>$model->id,'status'=>0])->count();
						if($countVolunteer>0){								
							return Html::a($countVolunteer." Volunteer Menunggu Konfirmasi", ["list-volunteer", "id" => $model->id], ["class" => "btn btn-primary", "title" => "Detail"]);
						}else{
							return Html::a("<i class='fa fa-list'></i>", ["list-volunteer", "id" => $model->id], ["class" => "btn btn-primary", "title" => "Detail"]);
						}
                    },
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
                    'format' => 'raw',
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>
