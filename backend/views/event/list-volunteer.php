<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use common\models\EventVolunteer;
use common\models\Event;
use common\models\USer;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Volunteer';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'List Volunteer';

$getEvent=Event::findOne($id);
?>
<h1>
    <?=$getEvent->nama?>
	<?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default pull-right']) ?>
</h1>
<?php if($getEvent->kuota<=$getEvent->jumlah_volunteer){?>
<div class="box box-danger">
	<div class="box-header">
		<h3 class="text-danger">Kuota Volunteer Telah mencapai limit</h3>
	</div>
</div>
<?php }?>
<div class="box box-info">
    <div class="box-body">
        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>

        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'],
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
            'headerRowOptions' => ['class' => 'x'],
            'columns' => [
				[
                    'class' => 'yii\grid\SerialColumn',
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:50px'],
                ],
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => 'Volunteer',
					'label'=>'Volunteer',
					'value' => function ($model) {
						return Html::button($model->user->nama_lengkap,["class" => "btn btn-default btn-change-aktif", "title" => "menunggu",'data-toggle'=>"modal",'data-target'=>"#exampleModalUser$model->user_id"]);
					},
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:left;'],
					'format' => 'raw',
				],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'status',
					'label'=>'Status',
                    'value' => function ($model) {
						if($model->status==0){								
							return Html::button("Menunggu Konfirmasi",["class" => "btn btn-default btn-change-aktif", "title" => "menunggu"]);
						}else if($model->status==1){								
							return Html::button("Permintaan Bergabung Diterima",["class" => "btn btn-success btn-change-nonaktif", "title" => "diterima"]);
						}else{
							return Html::button("Permintaan Bergabung Ditolak",["class" => "btn btn-danger btn-change-nonaktif", "title" => "ditolak"]);
						}
                    },
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
                    'format' => 'raw',
                ],
				[
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'Action',
					'label'=>'action',
                    'value' => function ($model) {
						if($model->status==0){								
							$event=Event::findOne($model->event_id);
							if($event->kuota<=$event->jumlah_volunteer){								
								return Html::button("<i class='fa fa-check'></i>",["class" => "btn btn-success btn-change-aktif", "title" => "Tidak aktif",'disabled'=>true]).' '.Html::button("<i class='fa fa-times'></i>",["class" => "btn btn-danger btn-change-aktif", "title" => "Tidak aktif"]);
							}else{
								return Html::a("<i class='fa fa-check'></i>",['terima-volunteer','id'=>$model->id],["class" => "btn btn-success btn-change-aktif", "title" => "Tidak aktif"]).' '.Html::a("<i class='fa fa-times'></i>",['tolak-volunteer','id'=>$model->id],["class" => "btn btn-danger btn-change-aktif", "title" => "Tidak aktif"]);
							}
						}else{
							return '';
						}
                    },
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
                    'format' => 'raw',
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
		<?php foreach($model as $eventVolunteer){?>
			<div class="modal fade" id="exampleModalUser<?=$eventVolunteer->user_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel">Detail Volunteer
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</h3>
						</div>
						<div class="modal-body">
							<?php $user=User::findOne($eventVolunteer->user_id);?>
							<div class="form-group has-feedback" style="display:flex;">
								<label for="foto" id="label_foto" style="position:relative;cursor: pointer;box-shadow: 0px 0px 6px #00000029;width:100px;height:100px;border-radius:50%;overflow:hidden;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$user->foto);?>);background-position:center;background-size:cover;">
								</label>												
							</div>
							<?= DetailView::widget([
								'model' => $user,
								'attributes' => [
									[
										'label'  => 'Nama Lengkap',
										'value'  => $user->nama_lengkap,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Email',
										'value'  => $user->email,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'No Identitas',
										'value'  => $user->no_identitas,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Tempat Lahir',
										'value'  => $user->tempat_lahir,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Tanggal Lahir',
										'value'  => date("d F Y",strtotime($user->tanggal_lahir)),
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Jenis Kelamin',
										'value'  => $user->jenis_kelamin,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Nomor Telepon',
										'value'  => $user->no_telp,
										'captionOptions' => ['style' => 'width:200px'],
									],
									[
										'label'  => 'Alamat Lengkap',
										'value'  => $user->alamat_lengkap,
										'captionOptions' => ['style' => 'width:200px'],
									],
								],
							]) ?>
						</div>
					</div>
				</div>
			</div>
		<?php }?>
    </div>
</div>
