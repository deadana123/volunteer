<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = 'Gallery - Tambah Data';
$this->params['breadcrumbs'][] = "Master";
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $id, 'url' => ['view','id'=>$id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-body">
        <?php echo $this->render('_form_gallery', [
            'model' => $model,
        ]); ?>
    </div>
</div>
