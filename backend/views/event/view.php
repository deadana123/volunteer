<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').attr('src', e.target.result);
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#eventfoto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
<div class="box box-info">
    <div class="box-body">

		<p>
			<?= Html::a("<i class='fa fa-arrow-left'></i> Kembali", ['index'], ['class' => 'btn btn-default']) ?>
		</p>
		<hr/>
		<div class="col-md-12" style="display:flex;justify-content:center;align-items:center;">
			<img id="foto" style="width:50%;height:300px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>">
		</div>
		<div class="col-md-12">
			<h2>Detail</h2>
			<hr/>
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'nama:ntext',
					[
						'label'  => 'Deskripsi',
						'value'  => $model->isi,
						'captionOptions' => ['style' => 'width:200px'],
					],
					[
						'label'  => 'Kategori',
						'value'  => $model->eventKategori->nama
					],
					[
						'label'  => 'Kabupaten, Provinsi',
						'value'  => $model->wilayahKabupaten->ibukota.', '.$model->wilayahKabupaten->wilayahProvinsi->nama
					],
					'alamat_lengkap',
					[
						'label'  => 'Mulai Event',
						'value'  => date('d F Y',strtotime($model->mulai)).' '.date('H:i',strtotime($model->waktu_mulai))
					],
					[
						'label'  => 'Selesai Event',
						'value'  => date('d F Y',strtotime($model->selesai)).' '.date('H:i',strtotime($model->waktu_selesai))
					],
					'view',
					'status',
				],
			]) ?>
		</div>
		<div class="col-md-12">
			<hr/>
			<h2 style="display:inline;">Foto
				<?= Html::a("<i class='fa fa-plus'></i> Tambah Foto", ['index'], ['class' => 'btn btn-success pull-right']) ?>
			</h2>
			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<?php $form = ActiveForm::begin([
								'id' => 'Event',
								'layout' => 'horizontal',
								'enableClientValidation' => false,
								'errorSummaryCssClass' => 'error-summary alert alert-error',
								'options' => ['enctype' => 'multipart/form-data'],
							]
						);
						?>
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel">Tambah Foto
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</h3>
						</div>
						<div class="modal-body">
							<label for="eventfoto" style="cursor: pointer;box-shadow: 0px 0px 6px #00000029;height: 200px;width: 100%;">
								<img id="label_foto" style="display:none;width:100%;height:100%;padding:20px;">
								<img id="label_foto_icon" style="width: 100px;text-align: center;justify-content: center;align-items: center;display: flex;vertical-align: middle;margin: 80px auto 0;" src="<?=Url::to(['/picture-upload.png'])?>">
							</label>
							<input type="file" name="eventfoto" id="eventfoto" style="display:none;">
						</div>
						<div class="modal-footer">
							<?php echo $form->errorSummary($model); ?>
							<?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
						</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
			<hr/>
			
			<div class="col-md-3">
				<div data-toggle="modal" data-target="#exampleModal" style="border:3px dashed #000;padding:10px;cursor:pointer;text-align:center;max-height:200px;min-height:200px;display: flex;align-items: center;justify-content: center;">
					<h1><i class="fa fa-plus" style="text-align:center;"></i></h1>
				</div>
			</div>
			<?php foreach(common\models\EventFoto::find()->where(['event_id'=>$model->id])->all() as $eventFoto){?>
				<div class="col-md-3">
					<div data-toggle="modal" data-target="#exampleModal<?=$eventFoto->id?>" style="max-height:200px;min-height:200px;box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);border-radius:20px;">
						<img id="foto" style="width:100%;height:200px;border-radius:20px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$eventFoto->foto);?>">
						
					</div>
				</div>
				<div class="modal fade" id="exampleModal<?=$eventFoto->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title" id="exampleModalLabel">Preview
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</h3>
							</div>
							<div class="modal-body">
								<label style="cursor: pointer;box-shadow: 0px 0px 6px #00000029;width: 100%;">
									<img style="width:100%;height:100%;padding:20px;text-align: center;justify-content: center;align-items: center;display: flex;vertical-align: middle;" src="<?=Yii::getAlias("@frontend_url/uploads/".$eventFoto->foto);?>">
								</label>
							</div>
							<div class="modal-footer">
								<?= Html::a("<i class='fa fa-trash'></i> Delete", ["delete-event-foto", "id" => $eventFoto->id,"event_id" => $eventFoto->event_id], [
									"class" => "btn btn-danger",
									"title" => "Hapus Data",
									"data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
									//"data-method" => "GET"
								]) ?>
							</div>
						</div>
					</div>
				</div>
			<?php }?>
		</div>
	</div>
</div>
