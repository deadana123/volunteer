<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
        'id' => 'Event',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
		'options' => ['enctype' => 'multipart/form-data'],
    ]
);
?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').attr('src', e.target.result);
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#foto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
<div class="form-group">
	<label class="control-label col-md-3" for="foto">Foto</label>
	<div class="col-md-6">
		<label for="foto" style="cursor: pointer;box-shadow: 0px 0px 6px #00000029;height: 200px;width: 100%;">
			<?php if($model->foto!=''){?>
				<img id="label_foto" style="display:none;width:100%;height:100%;padding:20px;">
				<img id="label_foto_icon" style="width:100%;height:100%;padding:20px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>">
			<?php }else{?>
				<img id="label_foto" style="display:none;width:100%;height:100%;padding:20px;">
				<img id="label_foto_icon" style="width: 100px;text-align: center;justify-content: center;align-items: center;display: flex;vertical-align: middle;margin: 80px auto 0;" src="<?=Url::to(['/picture-upload.png'])?>">
			<?php }?>
		</label>
		<input type="file" name="foto" id="foto" style="display:none;">
	</div>
</div>
<?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'isi')->textArea(['rows' => 6])->label('Keterangan') ?>
<?= $form->field($model, 'event_kategori_id')->dropDownList(
    \yii\helpers\ArrayHelper::map(common\models\EventKategori::find()->all(), 'id', 'nama'),
    ['prompt' => 'Select']
)->label('Kategori Event'); ?>
<?= $form->field($model, 'wilayah_kabupaten_id')->dropDownList(
    \yii\helpers\ArrayHelper::map(common\models\WilayahKabupaten::find()->all(), 'id', 'nama'),
    ['prompt' => 'Select']
)->label('Kabupaten'); ?>
<?= $form->field($model, 'alamat_lengkap')->textArea(['rows' => 6]) ?>
<div class="form-group">
    <label class="control-label col-md-3" for="events-mulai">Mulai Event</label>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?=Html::activeInput('date', $model, 'mulai', ['class' => 'form-control','required'=>true]);?>
            </div>
            <div class="col-md-6">
                <?=Html::activeInput('time', $model, 'waktu_mulai', ['class' => 'form-control','required'=>true,'min'=>date('Y-m-d')]);?>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
	<label class="control-label col-md-3" for="events-selesai">Seleai Event</label>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <?=Html::activeInput('date', $model, 'selesai', ['class' => 'form-control','required'=>true,'min'=>date('Y-m-d')]);?>
            </div>
            <div class="col-md-6">
                <?=Html::activeInput('time', $model, 'waktu_selesai', ['class' => 'form-control','required'=>true]);?>
            </div>
        </div>
    </div>
</div>
<hr/>
<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-offset-3 col-md-7">
        <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
        <?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

