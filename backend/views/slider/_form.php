<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<?php $form = ActiveForm::begin([
        'id' => 'Slider',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'options' => ['enctype' => 'multipart/form-data'],
    ]
);
?>

<?= $form->field($model, 'foto')->widget(\kartik\file\FileInput::className(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'gif', 'bmp'],
    ],
]) ?>
<?php
if ($model->foto != null) {
    ?>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::img(Yii::getAlias("@frontend_url/uploads/".$model->foto), ["width" => "150px"]); ?>
        </div>
    </div>
    <?php
}
?>

<hr/>
        <?php // $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-offset-3 col-md-7">
        <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
        <?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
