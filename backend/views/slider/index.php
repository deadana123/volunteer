<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Slider;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventKategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Slider';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<div class="box box-info">
    <div class="box-body">
        <?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>

        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'],
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
            'headerRowOptions' => ['class' => 'x'],
            'columns' => [

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-eye'></i>", ["view", "id" => $model->id], ["class" => "btn btn-success", "title" => "Lihat Data"]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-pen'></i>", ["update", "id" => $model->id], ["class" => "btn btn-warning", "title" => "Edit Data"]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-trash'></i>", ["delete", "id" => $model->id], [
                                "class" => "btn btn-danger",
                                "title" => "Hapus Data",
                                "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                                "data-method" => "POST"
                            ]);
                        },
                        'role-menu' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-cog'></i>", ["detail", "id" => $model->id], ["class" => "btn btn-info", "title" => "Detail"]);
                        },
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:140px']
                ],
				[
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'Foto',
					'label'=>'foto',
                    'value' => function ($model) {
						return Html::img(Yii::getAlias("@frontend_url/uploads/".$model->foto), ["width" => "150px",'data-toggle'=>"modal",'data-target'=>"#exampleModalSlider$model->id"]);
                    },
					'contentOptions' => ['nowrap' => 'nowrap'],
                    'format' => 'raw',
                ],
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>
<?php foreach(Slider::find()->all() as $slider){?>
	<div class="modal fade" id="exampleModalSlider<?=$slider->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel">Preview
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</h3>
				</div>
				<div class="modal-body">
					<label for="foto" id="label_foto" style="position:relative;cursor: pointer;box-shadow: 0px 0px 6px #00000029;min-height:300px;width:100%;overflow:hidden;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$slider->foto);?>);background-position:center;background-size:cover;">
					</label>												
				</div>
			</div>
		</div>
	</div>
<?php }?>
