<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HubungiKami */

$this->title = 'pesan oleh :'.$model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Hubungi Kami', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box box-info">
	<div class="box-body">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'nama',
				[
					'label'  => 'No Telepon',
					'value'  => $model->no_telp,
					'captionOptions' => ['style' => 'width:140px'],
				],
				'email:email',
				[
					'label'  => 'Pesan',
					'value'  => $model->pesan,
				],
				[
					'label'  => 'Pesan dikirim',
					'value'  => date("d F Y H:i:s",strtotime($model->created_at)),
					'captionOptions' => ['style' => 'width:140px'],
				],
			],
		]) ?>
	</div>
</div>
