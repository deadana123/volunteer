<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsKategori */

$this->title = 'Tambah Data';
$this->params['breadcrumbs'][] = "Master";
$this->params['breadcrumbs'][] = ['label' => 'Kategori News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-body">
        <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>
    </div>
</div>