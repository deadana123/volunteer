<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsKategori */

$this->title = 'Update News Kategori: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kategori News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-body">
        <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>
    </div>
</div>

