<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Stok $model
*/

$this->title = Yii::t('models', 'Stok');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Stoks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud stok-create">

    <h1>
        <?= Yii::t('models', 'Stok') ?>
        <small>
                        <?= Html::encode($model->id) ?>
        </small>
    </h1>

    <div class="clearfix crud-navigation">
        <div class="pull-left">
            <?=             Html::a(
            'Cancel',
            \yii\helpers\Url::previous(),
            ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <hr />

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
