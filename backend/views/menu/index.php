<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var common\models\search\MenuSearch $searchModel
*/

$this->title = 'Manajemen Menu';
$this->params['breadcrumbs'][] = $this->title;

//$this->registerJsFile(Url::to(['js/bootstrap-iconpicker.bundle.min.js']), ['position' => \yii\web\View::POS_END]);
?>

<style>
    .sorterer {
        text-align: center;
        background: #0000aa;
        color: #ffffff;
        cursor: move;
    }
    table tr.sorting-row td {background-color: #8b8;}
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-info">
            <div class="box-header">
                <?= Html::a("<i class=\"fa fa-plus\"></i> Tambah Menu Baru", ["create"], ["class"=>"btn btn-info"]) ?>
                <button id="simpanBtn" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            </div>
            <div class="box-body">
                <table class="table table-responsive" id="tableSorter">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Menu</th>
                        <th>Controller</th>
                        <th>Ikon</th>
                        <th>Induk</th>
                        <th style="width: 50px">#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $parents = \yii\helpers\ArrayHelper::map(\common\models\Menu::find()->where(["parent_id"=>null])->orderBy("`order` ASC")->all(), "id", "name");

                    $no = 1;
                    /* @var $menu \common\models\Menu*/
                    foreach(\common\models\Menu::find()->where(["parent_id"=>null])->orderBy("`order` ASC")->all() as $menu){
                        $name = Html::textInput("name", $menu->name, ["class"=>"form-control name"]);
                        $controller = Html::textInput("controller", $menu->controller, ["class"=>"form-control controller"]);
                        $parent = Html::dropDownList("parent_id", $menu->parent_id, $parents, ["class"=>"form-control parent_id", "prompt"=>"-"]);
                        $button = "<i class='fas fa-arrows-alt'></i>";
                        $icp = Html::textInput("icon", $menu->icon, ["class"=>"form-control icon icp-auto", "role"=>"iconpicker"]);
                        //$icp = Html::button("<i class='fas fa-chevron-down'></i>", ["class"=>"btn btn-info", "role"=>"iconpicker"]);
                        echo "<tr style='background-color: #FFFCE7;' data='{$menu->id}'>
                            <td>{$no}</td>
                            <td>{$name}</td>
                            <td>{$controller}</td>
                            <td>{$icp}</td>
                            <td>{$parent}</td>
                            <td class='sorterer'>{$button}</td>
                            </tr>";
                        $no ++;
                        foreach(\common\models\Menu::find()->where(["parent_id"=>$menu->id])->orderBy("`order` ASC")->all() as $menu2){
                            $name = Html::textInput("name", $menu2->name, ["class"=>"form-control name"]);
                            $controller = Html::textInput("controller", $menu2->controller, ["class"=>"form-control controller"]);
                            $parent = Html::dropDownList("parent_id", $menu2->parent_id, $parents, ["class"=>"form-control parent_id", "prompt"=>"-"]);
                            $button = "<i class='fas fa-arrows-alt'></i>";
                            $icp = Html::textInput("icon", $menu2->icon, ["class"=>"form-control icon icp-auto"]);
                            //$icp = Html::button("ICON", ["class"=>"btn btn-danger", "role"=>"iconpicker"]);
                            echo "<tr data='{$menu2->id}'>
                            <td>{$no}</td>
                            <td>{$name}</td>
                            <td>{$controller}</td>
                            <td>{$icp}</td>
                            <td>{$parent}</td>
                            <td class='sorterer'>{$button}</td>
                            </tr>";
                            $no ++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('

new RowSorter("#tableSorter", {
    handler: "td.sorterer",
});

$("#simpanBtn").click(function(){
    var arr = [];
    $("tbody tr").each(function(){
        var obj = [];
        obj.push($(this).attr("data"));
        obj.push($(this).find(".name").val());
        obj.push($(this).find(".controller").val());
        obj.push($(this).find(".parent_id").val());
        obj.push($(this).find(".icon").val());
        arr.push(obj.join("[=]"));
    });
    console.log(arr.join("||"));
    $.ajax({
        url : "'.Url::to(["save"]).'",
        data : {
            str : arr.join("||"),
        },
        type : "post",
        success : function(){
            alert("Menu berhasil disimpan");
        }
    });
    return false;
});

$(".icp-auto").iconpicker({
    hideOnSelect: true
});

');
?>