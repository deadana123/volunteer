<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Product $model
*/

$this->title = Yii::t('models', 'Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Product'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="box box-info">
    <div class="box-body">
    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>
    </div>
</div>
