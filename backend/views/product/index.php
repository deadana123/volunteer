<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
    * @var common\models\search\ProductSearch $searchModel
*/

$this->title = Yii::t('models', 'Products');
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
$actionColumnTemplate = implode(' ', $actionColumnTemplates);
    $actionColumnTemplateString = $actionColumnTemplate;
} else {
Yii::$app->view->params['pageButtons'] = Html::a('<span class="glyphicon glyphicon-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']);
    $actionColumnTemplateString = "{view} {update} {delete}";
}
$actionColumnTemplateString = '<div class="action-buttons">'.$actionColumnTemplateString.'</div>';
?>
<div class="box box-info">
    <div class="box-body">

    <?php
//             echo $this->render('_search', ['model' =>$searchModel]);
        ?>

    
    <?php \yii\widgets\Pjax::begin(['id'=>'pjax-main', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>

    <h1>
        <?= Yii::t('models', 'Products') ?>
        <small>
            List
        </small>
    </h1>
    <div class="clearfix crud-navigation">
        <div class="pull-left">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . 'New', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

        <div class="pull-right">

                        
            <?= 
            \yii\bootstrap\ButtonDropdown::widget(
            [
            'id' => 'giiant-relations',
            'encodeLabel' => false,
            'label' => '<span class="glyphicon glyphicon-paperclip"></span> ' . 'Relations',
            'dropdown' => [
            'options' => [
            'class' => 'dropdown-menu-right'
            ],
            'encodeLabels' => false,
            'items' => [

]
            ],
            'options' => [
            'class' => 'btn-default'
            ]
            ]
            );
            ?>
        </div>
    </div>

    <hr />

    <div class="table-responsive">
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
        'class' => yii\widgets\LinkPager::className(),
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last',
        ],
                    'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
        'headerRowOptions' => ['class'=>'x'],
        'columns' => [
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} {view}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-eye'></i>", ["view", "id" => $model->id], ["class" => "btn btn-success", "title" => "Lihat Data"]);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-pencil-alt'></i> update", ["update", "id" => $model->id], ["class" => "btn btn-warning", "title" => "Edit Data"]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-trash'></i>", ["delete", "id" => $model->id], [
                                "class" => "btn btn-danger",
                                "title" => "Hapus Data",
                                "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                                //"data-method" => "GET"
                            ]);
                        },
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:140px']
                ],
                'nama',
			     'harga',
        ],
        ]); ?>
    </div>

    </div>
</div>


<?php \yii\widgets\Pjax::end() ?>


