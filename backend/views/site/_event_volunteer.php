<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\EventVolunteer;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="box box-info">
	<div class="box-header"><h3>Data Volunteer</h3><hr/></div>
    <div class="box-body">
		<?= GridView::widget([
			'layout' => '{summary}{pager}{items}{pager}',
			'dataProvider' => $dataProvider,
			'pager' => [
				'class' => yii\widgets\LinkPager::className(),
				'firstPageLabel' => 'First',
				'lastPageLabel' => 'Last'],
		//	'filterModel' => $searchModel,
			'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
			'headerRowOptions' => ['class' => 'x'],
			'columns' => [
				[
					'class' => 'yii\grid\SerialColumn',
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:50px']
				],
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => 'volunteer',
					'label'=>'Volunteer',
					'value' => function ($model) {
						return $model->user->nama_lengkap;
					},
				],
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => 'event',
					'label'=>'Event',
					'value' => function ($model) {
						return $model->event->nama;
					},
				],
			],
		]); ?>
	</div>
</div>