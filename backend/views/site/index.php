<?php

use common\models\Event;
use common\models\EventVolunteer;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
date_default_timezone_set('Asia/Jakarta'); 
$this->title = 'Beranda';

if(isset($_GET['start'])){	
	$start = $_GET['start'];
}
if (!isset($start)) {
    $start = date("Y-m-d",strtotime("-1 months"));
}

if(isset($_GET['end'])){	
	$end = $_GET['end'];
}
if (!isset($end)) {
    $end = date("Y-m-d");
}


?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row" style="margin-bottom: 20px">
                    <div class="col-md-2">
                        <?= Html::textInput("start_date", $start, ["value"=>$start,"type" => "date", "class" => "form-control", "id" => "start_date"]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::textInput("end_date", $end, ["value"=>$end,"type" => "date", "class" => "form-control", "id" => "end_date"]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box">
			<div class="box-header"><h3>Jumlah Event</h3><hr/></div>
            <div class="box-body">
                <div id="container" style="height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
	<div class="col-md-6">
        <div class="box">
			<div class="box-header"><h3>Jumlah Volunter Bergabung</h3><hr/></div>
            <div class="box-body">
                <div id="container2" style="height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
		<div id="output"></div>
    </div>
	<div class="col-md-12">
		<div id="output2"></div>
    </div>
</div>
<?php
$tanggal = [];
$tanggalDisplay = [];
for ($i = strtotime($start); $i <= strtotime($end); $i += 3600 * 24) {
    $tanggal[] = date("Y-m-d", $i);
    $tanggalDisplay[] = date("d M", $i);
}

$webVisit = [];
$pageVisit = [];
foreach ($tanggal as $tgl) {
    $webVisit[] = intval(Event::find()->where(['status'=>1])->andWhere(['between','date(waktu_keputusan_admin)',$start,$tgl])->count());
    $volunteerJoin[] = intval(EventVolunteer::find()->where(['status'=>1])->andWhere(['between','date(waktu_keputusan_admin)',$start,$tgl])->count());
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $("#start_date").change(function () {
        window.location = "<?= Url::to(["site/index"]) ?>?start=" + $("#start_date").val() + "&end=" + $("#end_date").val();
		$("#output").load("<?= Url::to(['site/get-data-event']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
		$("#output2").load("<?= Url::to(['site/get-data-volunteer']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
		return false;
    });
    $("#end_date").change(function () {
        window.location = "<?= Url::to(["site/index"]) ?>?start=" + $("#start_date").val() + "&end=" + $("#end_date").val();
		$("#output").load("<?= Url::to(['site/get-data-event']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
		$("#output2").load("<?= Url::to(['site/get-data-volunteer']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
        return false;
    });

    $(document).ready(function () {
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?= \yii\helpers\Json::encode($tanggalDisplay) ?>
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Jumlah Event',
                data:  <?= \yii\helpers\Json::encode($webVisit) ?>
            }]
        });
		
		Highcharts.chart('container2', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?= \yii\helpers\Json::encode($tanggalDisplay) ?>
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Jumlah Volunter Bergabung',
                data:  <?= \yii\helpers\Json::encode($volunteerJoin) ?>
            }]
        });
		$("#output").load("<?= Url::to(['site/get-data-event']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
		$("#output2").load("<?= Url::to(['site/get-data-volunteer']) ?>?start=" + $("#start_date").val()+ "&end=" + $("#end_date").val());
    });
</script>