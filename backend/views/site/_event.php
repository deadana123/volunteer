<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\EventVolunteer;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="box box-info">
	<div class="box-header"><h3>Data Event</h3><hr/></div>
    <div class="box-body">
		<?= GridView::widget([
			'layout' => '{summary}{pager}{items}{pager}',
			'dataProvider' => $dataProvider,
			'pager' => [
				'class' => yii\widgets\LinkPager::className(),
				'firstPageLabel' => 'First',
				'lastPageLabel' => 'Last'],
		//	'filterModel' => $searchModel,
			'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
			'headerRowOptions' => ['class' => 'x'],
			'columns' => [
				[
					'class' => 'yii\grid\SerialColumn',
					'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:50px']
				],
				'nama',
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => 'created_by',
					'label'=>'Created By',
					'value' => function ($model) {
						return $model->user->nama_lengkap;
					},
				],
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => '',
					'label'=>'',
					'value' => function ($model) {						
						return Html::a("Detail Event", ["/event/view", "id" => $model->id], ["class" => "btn btn-success", "title" => "Detail"]);
					},
					'format' => 'raw',
				],
				[
					'class' => yii\grid\DataColumn::className(),
					'attribute' => '',
					'label'=>'',
					'value' => function ($model) {
						$countVolunteer = EventVolunteer::find()->where(['event_id'=>$model->id,'status'=>0])->count();
						if($countVolunteer>0){								
							return Html::a($countVolunteer." Volunteer Menunggu Konfirmasi", ["/event/list-volunteer", "id" => $model->id], ["class" => "btn btn-primary", "title" => "Detail"]);
						}else{
							return Html::a("<i class='fa fa-list'></i>", ["/event/list-volunteer", "id" => $model->id], ["class" => "btn btn-primary", "title" => "Detail"]);
						}
					},
					'format' => 'raw',
				],
			],
		]); ?>
	</div>
</div>