<?php

namespace backend\controllers;

use Yii;
use common\models\News;
use common\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\NodeLogger;
/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->where(['is_deleted'=>0]);
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		date_default_timezone_set("Asia/Jakarta");
        $model = new News();
		$model->view = 0;
		$model->created_at=date("Y-m-d H:i:s");
		$model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {	
			if($_FILES['foto']['name']!=''){				
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}else{
				$model->foto = 'default-upload.png';
			}
			
			if($_POST['News']['nama']=='' || $_POST['News']['isi']=='' || $_POST['News']['news_kategori_id']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				$model->save();
				return $this->redirect(['index']);
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        date_default_timezone_set("Asia/Jakarta");
        $model = $this->findModel($id);
		$old_foto=$model->foto;
		$model->updated_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {
			if(isset($_FILES)){				
				if($_FILES['foto']['name']!=''){
					$foto=$_FILES['foto'];
					$arr = explode(".", $foto['name']);
					$extension = end($arr);

					# generate a unique file name
					$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

					# the path to save file
					$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
					move_uploaded_file($foto['tmp_name'], $path);
					
					//resize image
					//$image = new ImageResize($path);
					//$image->resizeToLongSide(683);
					//$image->save($path);
				}
			}else{
				$model->foto = $old_foto;
			}

			if($_POST['News']['nama']=='' || $_POST['News']['isi']=='' || $_POST['News']['news_kategori_id']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				$model->save();
				return $this->redirect(['index']);
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
		$model->is_deleted = 1;
		$model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
