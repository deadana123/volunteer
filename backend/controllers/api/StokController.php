<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "StokController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class StokController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Stok';
}
