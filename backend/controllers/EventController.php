<?php

namespace backend\controllers;

use Yii;
use common\models\Event;
use common\models\EventVolunteer;
use common\models\search\EventSearch;
use common\models\search\EventVolunteerSearch;
use common\models\EventFoto;
use common\models\search\EventFotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->where(['is_deleted'=>0]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionTerimaEvent($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = $this->findModel($id);
		$model->status=1;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
		
		
        return $this->redirect(['index']);
    }
	
	public function actionTolakEvent($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = $this->findModel($id);
		$model->status=2;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
        return $this->redirect(['index']);
    }
	
	public function actionListVolunteer($id)
    {
        $searchModel = new EventVolunteerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->where(['event_id'=>$id]);
		$model=EventVolunteer::find()->where(['event_id'=>$id])->all();

        return $this->render('list-volunteer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model'=>$model,
			'id'=>$id,
        ]);
    }
	
	public function actionTerimaVolunteer($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = EventVolunteer::findOne($id);
		$model->status=1;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
		
		$updateEvent = Event::findOne($model->event_id);
		$updateEvent->jumlah_volunteer+=1;
		$updateEvent->save();
		
        return $this->redirect(['list-volunteer','id'=>$model->event_id]);
    }
	
	public function actionTolakVolunteer($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = EventVolunteer::findOne($id);
		$model->status=2;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
        return $this->redirect(['list-volunteer','id'=>$model->event_id]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {	
		$model = new EventFoto();
		$model->event_id=$id;

		if(isset($_FILES['eventfoto']['name'])){			
			if($_FILES['eventfoto']['name']!=''){			
				$foto=$_FILES['eventfoto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
				$model->save();
				return $this->redirect(['view','id'=>$id]);
			}
		}
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	public function actionGallery($event_id)
    {
		$searchModel = new EventFotoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->where(['event_id'=>$event_id]);

        return $this->render('gallery', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'event_id'=>$event_id
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		date_default_timezone_set("Asia/Jakarta");
        $model = new Event();
		$model->user_id = Yii::$app->user->id;
		$model->status = 0;
		$model->view = 0;
		$model->created_at=date("Y-m-d H:i:s");
		$model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {	
			if($_FILES['foto']['name']!=''){				
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}else{
				$model->foto = 'default-upload.png';
			}
			
			if($_POST['Event']['nama']=='' || $_POST['Event']['isi']=='' || $_POST['Event']['event_kategori_id']=='' || $_POST['Event']['wilayah_kabupaten_id']=='' || $_POST['Event']['alamat_lengkap']=='' || $_POST['Event']['mulai']=='' || $_POST['Event']['waktu_mulai']=='' || $_POST['Event']['selesai']=='' || $_POST['Event']['waktu_selesai']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				$model->save();
				return $this->redirect(['index']);
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateGallery($id)
    {
		date_default_timezone_set("Asia/Jakarta");
        $model = new EventFoto();
        if ($model->load(Yii::$app->request->post())) {	
			if($_FILES['foto']['name']!=''){			
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}else{
				$model->foto = 'default-upload.png';
			}

			$model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	/**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		date_default_timezone_set("Asia/Jakarta");
        $model = $this->findModel($id);
		$old_foto=$model->foto;
		$model->updated_at=date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post())) {
			if(isset($_FILES)){				
				if($_FILES['foto']['name']!=''){
					$foto=$_FILES['foto'];
					$arr = explode(".", $foto['name']);
					$extension = end($arr);

					# generate a unique file name
					$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

					# the path to save file
					$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
					move_uploaded_file($foto['tmp_name'], $path);
					
					//resize image
					//$image = new ImageResize($path);
					//$image->resizeToLongSide(683);
					//$image->save($path);
				}
			}else{
				$model->foto = $old_foto;
			}

			if($_POST['Event']['nama']=='' || $_POST['Event']['isi']=='' || $_POST['Event']['event_kategori_id']=='' || $_POST['Event']['wilayah_kabupaten_id']=='' || $_POST['Event']['alamat_lengkap']=='' || $_POST['Event']['mulai']=='' || $_POST['Event']['waktu_mulai']=='' || $_POST['Event']['selesai']=='' || $_POST['Event']['waktu_selesai']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				$model->save();
				return $this->redirect(['index']);
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		$model->is_deleted = 1;
		$model->save();
        return $this->redirect(['index']);
    }
	
	public function actionDeleteEventFoto($id,$event_id)
    {
        EventFoto::findOne($id)->delete();

        return $this->redirect(['view','id'=>$event_id]);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
