<?php

namespace backend\controllers;

use \Yii;
use common\models\User;
use common\models\search\UserSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;
use dmstr\bootstrap\Tabs;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    
	
	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel  = new UserSearch;
		$dataProvider = $searchModel->search($_GET);
		$dataProvider->query->where(['is_deleted'=>0]);

		Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single User model.
	 * @param integer $id
     *
	 * @return mixed
	 */
	public function actionView($id)
	{
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new User;

		try {
            date_default_timezone_set("Asia/Jakarta");
            $model->status = 1;
            $model->created_at = date("Y-m-d H:i:s");
            if ($model->load($_POST)) {

                $image = UploadedFile::getInstance($model, 'foto');
                if ($image != NULL) {
                    # store the source file name
                    $model->foto = $image->name;
                    $tmp           = explode('.', $image->name);
                    $extension = end($tmp);

                    # generate a unique file name
                    $model->foto = Yii::$app->security->generateRandomString() . ".{$extension}";

                    # the path to save file
                    $path = Yii::getAlias("@frontend/web/uploads/") . $model->foto;
                    $image->saveAs($path);
                }else{
                    $model->foto = "default.png";
                }
				
				if($_POST['User']['username']=='' || $_POST['User']['password']=='' || $_POST['User']['email']=='' || $_POST['User']['nama_lengkap']=='' || $_POST['User']['role_id']==''){
					\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
				}else{				
					$model->password = md5($model->password);
					$model->save();
					return $this->redirect(['index']);
				}
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model]);
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

        date_default_timezone_set("Asia/Jakarta");
        $oldMd5Password = $model->password;
        $oldfoto = $model->foto;

        $model->password = "";
        $model->updated_at = date("Y-m-d H:i:s");

        if ($model->load($_POST)){
            //password
            

            # get the uploaded file instance
            $image = UploadedFile::getInstance($model, 'foto');
            if ($image != NULL) {
                # store the source file name
                $model->foto = $image->name;
                $arr = explode(".", $image->name);
                $extension = end($arr);

                # generate a unique file name
                $model->foto = Yii::$app->security->generateRandomString() . ".{$extension}";

                # the path to save file
                $path = Yii::getAlias("@frontend/web/uploads/") . $model->foto;
                $image->saveAs($path);
            }else{
                $model->foto = $oldfoto;
            }

            if($_POST['User']['username']=='' || $_POST['User']['email']=='' || $_POST['User']['nama_lengkap']=='' || $_POST['User']['role_id']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				if($model->password != ""){
					$model->password = md5($model->password);
				}else{
					$model->password = $oldMd5Password;
				}
				
				$model->save();
				return $this->redirect(['index']);
			}
        }
        return $this->render('update', [
            'model' => $model,
        ]);
	}
	
	public function actionUserToAktif($id)
    {
		date_default_timezone_set("Asia/Jakarta");
        $model=User::findOne($id);
		$model->status=1;
		$model->updated_at=date("Y-m-d H:i:s");
		$model->save();
		
		return $this->redirect(['index']);
    }
	
	public function actionUserToNonaktif($id)
    {
		date_default_timezone_set("Asia/Jakarta");
        $model=User::findOne($id);
		$model->status=0;
		$model->updated_at=date("Y-m-d H:i:s");
		$model->save();
		
		return $this->redirect(['index']);
    }
	
	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->is_deleted=1;
		$model->save();
        return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
