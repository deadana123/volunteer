<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event_volunteer".
 *
 * @property int $id
 * @property int $event_id
 * @property int $user_id
 * @property string $waktu_keputusan_admin
 * @property int $status 0 = pengajuan,1 =  diterima, 2 = ditolak
 * @property string $created_at
 *
 * @property Event $event
 * @property User $user
 */
class EventVolunteer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_volunteer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'created_at'], 'required'],
            [['event_id', 'user_id', 'status'], 'integer'],
            [['waktu_keputusan_admin', 'created_at'], 'safe'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'user_id' => 'User ID',
            'waktu_keputusan_admin' => 'Waktu Keputusan Admin',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
