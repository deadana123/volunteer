<?php

namespace common\models;

use Yii;
use \common\models\base\Role as BaseRole;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "role".
 */
class Role extends BaseRole
{
	const SUPER_ADMINISTRATOR = 1;
    const ADMINISTRATOR = 2;
    const VOLUNTEER = 3;
    const COMPANY = 4;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
