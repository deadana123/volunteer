<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $nama
 * @property string $foto
 * @property string $isi
 * @property int $view
 * @property int $news_kategori_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $is_deleted 0 = konten aktif,1 = konten di hapus
 *
 * @property User $createdBy
 * @property User $createdBy0
 * @property NewsKategori $newsKategori
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'foto', 'isi', 'news_kategori_id', 'created_at', 'created_by'], 'required'],
            [['isi'], 'string'],
            [['view', 'news_kategori_id', 'created_by', 'is_deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 100],
            [['foto'], 'string', 'max' => 200],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['news_kategori_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsKategori::className(), 'targetAttribute' => ['news_kategori_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'foto' => 'Foto',
            'isi' => 'Isi',
            'view' => 'View',
            'news_kategori_id' => 'News Kategori ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsKategori()
    {
        return $this->hasOne(NewsKategori::className(), ['id' => 'news_kategori_id']);
    }
}
