<?php

namespace common\models;

use Yii;
use \common\models\base\Artikel as BaseArtikel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "artikel".
 */
class Artikel extends BaseArtikel
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
