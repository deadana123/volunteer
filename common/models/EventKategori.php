<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event_kategori".
 *
 * @property int $id
 * @property string $nama
 * @property string $created_at
 * @property int $is_deleted 0 = aktif,1 = di hapus
 *
 * @property Event[] $events
 */
class EventKategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['nama', 'created_at'], 'required'],
            [['created_at'], 'safe'],
            [['is_deleted'], 'integer'],
            [['nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'created_at' => 'Created At',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['event_kategori_id' => 'id']);
    }
}
