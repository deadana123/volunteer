<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hubungi_kami".
 *
 * @property int $id
 * @property string $nama
 * @property string $no_telp
 * @property string $email
 * @property string $pesan
 * @property string $created_at
 */
class HubungiKami extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hubungi_kami';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'no_telp', 'email', 'pesan'], 'required'],
            [['pesan'], 'string'],
            [['created_at'], 'safe'],
            [['nama'], 'string', 'max' => 200],
            [['no_telp'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'no_telp' => 'No Telp',
            'email' => 'Email',
            'pesan' => 'Pesan',
            'created_at' => 'Created At',
        ];
    }
}
