<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property int $user_id
 * @property string $nama
 * @property string $foto
 * @property string $isi
 * @property int $event_kategori_id
 * @property int $wilayah_kabupaten_id
 * @property string $alamat_lengkap
 * @property string $mulai
 * @property string $selesai
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property int $status
 * @property int $view
 * @property string $waktu_keputusan_admin
 * @property int $kuota 1
 * @property int $jumlah_volunteer
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $is_deleted 0 = akun aktif,1 = akun di hapus
 *
 * @property User $user
 * @property EventKategori $eventKategori
 * @property WilayahKabupaten $wilayahKabupaten
 * @property User $createdBy
 * @property EventFoto[] $eventFotos
 * @property EventVolunteer[] $eventVolunteers
 * @property Log[] $logs
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'event_kategori_id', 'wilayah_kabupaten_id', 'status', 'view', 'kuota', 'jumlah_volunteer', 'created_by', 'is_deleted'], 'integer'],
            [['nama', 'isi'], 'string'],
            [['mulai', 'selesai', 'waktu_mulai', 'waktu_selesai', 'waktu_keputusan_admin', 'created_at', 'updated_at'], 'safe'],
            [['kuota'], 'required'],
            [['foto', 'alamat_lengkap'], 'string', 'max' => 200],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['event_kategori_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventKategori::className(), 'targetAttribute' => ['event_kategori_id' => 'id']],
            [['wilayah_kabupaten_id'], 'exist', 'skipOnError' => true, 'targetClass' => WilayahKabupaten::className(), 'targetAttribute' => ['wilayah_kabupaten_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'nama' => 'Nama',
            'foto' => 'Foto',
            'isi' => 'Isi',
            'event_kategori_id' => 'Event Kategori ID',
            'wilayah_kabupaten_id' => 'Wilayah Kabupaten ID',
            'alamat_lengkap' => 'Alamat Lengkap',
            'mulai' => 'Mulai',
            'selesai' => 'Selesai',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'status' => 'Status',
            'view' => 'View',
            'waktu_keputusan_admin' => 'Waktu Keputusan Admin',
            'kuota' => 'Kuota',
            'jumlah_volunteer' => 'Jumlah Volunteer',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventKategori()
    {
        return $this->hasOne(EventKategori::className(), ['id' => 'event_kategori_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWilayahKabupaten()
    {
        return $this->hasOne(WilayahKabupaten::className(), ['id' => 'wilayah_kabupaten_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventFotos()
    {
        return $this->hasMany(EventFoto::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventVolunteers()
    {
        return $this->hasMany(EventVolunteer::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['event_id' => 'id']);
    }
}
