<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wilayah_propinsi".
 *
 * @property int $id
 * @property string $nama
 *
 * @property WilayahKabupaten[] $wilayahKabupatens
 */
class WilayahPropinsi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wilayah_propinsi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWilayahKabupatens()
    {
        return $this->hasMany(WilayahKabupaten::className(), ['wilayah_provinsi_id' => 'id']);
    }
}
