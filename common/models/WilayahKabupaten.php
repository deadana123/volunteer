<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wilayah_kabupaten".
 *
 * @property int $id
 * @property int $wilayah_provinsi_id
 * @property string $nama
 * @property string $ibukota
 *
 * @property Event[] $events
 * @property WilayahPropinsi $wilayahProvinsi
 */
class WilayahKabupaten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wilayah_kabupaten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wilayah_provinsi_id'], 'required'],
            [['wilayah_provinsi_id'], 'integer'],
            [['nama', 'ibukota'], 'string', 'max' => 100],
            [['wilayah_provinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => WilayahPropinsi::className(), 'targetAttribute' => ['wilayah_provinsi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wilayah_provinsi_id' => 'Wilayah Provinsi ID',
            'nama' => 'Nama',
            'ibukota' => 'Ibukota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['wilayah_kabupaten_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWilayahProvinsi()
    {
        return $this->hasOne(WilayahPropinsi::className(), ['id' => 'wilayah_provinsi_id']);
    }
}
