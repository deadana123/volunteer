<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 03/02/18
 * Time: 16.37
 */

namespace common\components;


use yii\helpers\Json;
use yii\web\JsonResponseFormatter;

class PrettyJsonResponseFormatter extends JsonResponseFormatter
{

    protected function formatJson($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        if ($response->data !== null) {
            $response->content = Json::encode($response->data, JSON_PRETTY_PRINT);
        }
    }
}