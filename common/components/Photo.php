<?php

namespace common\components;

use Yii;
use yii\helpers\Url;

class Photo
{    
    public static function get($file)
    {
        $file = trim($file);
        if (substr($file, 0, 4) == "http") {
            return $file;
        }
        if (file_exists(\Yii::getAlias("@frontend/web/uploads/" . $file)) && is_file(\Yii::getAlias("@frontend/web/uploads/" . $file))) {
            return Yii::getAlias("@frontend_url/uploads/".$file);
        } else {
            return Yii::getAlias("@frontend_url/default-upload.png");
        }
    }
}