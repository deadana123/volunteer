-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL DEFAULT 'index',
  `icon` varchar(50) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `name`, `controller`, `action`, `icon`, `order`, `parent_id`) VALUES
(1,	'Dashboard',	'site',	'index',	'fas fa-home',	1,	NULL),
(2,	'Master',	'',	'index',	'fa fa-database',	2,	NULL),
(3,	'Menu',	'menu',	'index',	'far fa-circle',	3,	2),
(4,	'Privilege',	'role',	'index',	'far fa-circle',	4,	2),
(5,	'Operator',	'user',	'index',	'far fa-circle',	5,	2),
(6,	'Company Setup',	'company-setup',	'index',	'fa-building',	12,	NULL),
(7,	'Parking Lot Management',	'location',	'index',	'fa-codepen',	11,	NULL),
(8,	'Customer Information',	'customer',	'index',	'far fa-user',	10,	NULL),
(9,	'Financial Management',	'finance',	'index',	'far fa-money-bill-alt',	9,	NULL),
(10,	'Expense',	'expense',	'index',	'far fa-money-bill-alt',	8,	NULL),
(11,	'Promo Code Generator',	'promo-code',	'index',	'far fa-address-card',	7,	NULL),
(12,	'Subscription Voucher',	'subscription',	'index',	'fas fa-ticket-alt',	6,	NULL),
(13,	'Payment Confirmation',	'payment-confirmation',	'index',	'fa-paperclip',	13,	NULL);

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `name`) VALUES
(1,	'Super Administrator'),
(2,	'Administrator'),
(3,	'Regular User'),
(4,	'test');

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`) VALUES
(114,	4,	1),
(115,	4,	2),
(116,	4,	3),
(117,	4,	4),
(118,	4,	5),
(119,	4,	6),
(120,	4,	7),
(121,	4,	8),
(122,	4,	9),
(123,	4,	10),
(124,	4,	11),
(125,	4,	12),
(126,	4,	13),
(177,	2,	1),
(178,	2,	2),
(179,	2,	3),
(180,	2,	4),
(181,	2,	5),
(205,	3,	1),
(206,	3,	2),
(207,	3,	3),
(208,	3,	4),
(209,	3,	5),
(210,	1,	1),
(211,	1,	2),
(212,	1,	3),
(213,	1,	4),
(214,	1,	5),
(215,	1,	7),
(216,	1,	8),
(217,	1,	9),
(218,	1,	10),
(219,	1,	11),
(220,	1,	12),
(221,	1,	13);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `photo_url` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `username`, `password`, `email`, `name`, `role_id`, `photo_url`, `last_login`, `last_logout`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	NULL,	'Super Administrator',	1,	NULL,	NULL,	NULL,	NULL,	NULL);

-- 2019-04-19 04:10:57
