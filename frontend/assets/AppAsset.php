<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        // 'css/style.css',
        'OwlCarousel/dist/assets/owl.carousel.min.css',
        'OwlCarousel/dist/assets/owl.theme.default.min.css',
		'css/fontawesome-iconpicker.min.css',
        'css/all.css',
    ];
    public $js = [
        'OwlCarousel/dist/owl.carousel.js',
		'js/fontawesome-iconpicker.min.js',
		'js/all.js',
		'js/rowsorter.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
