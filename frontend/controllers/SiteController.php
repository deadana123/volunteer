<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Event;
use common\models\News;
use common\models\Role;
use common\models\User;
use common\models\HubungiKami;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$limitEvent = Event::find()->where(['is_deleted'=>0,'status'=>1])->limit('6')->orderBy(['id' => SORT_DESC])->all();
		$limitNews1 = News::find()->where(['is_deleted'=>0])->limit('1')->orderBy(['id' => SORT_DESC])->all();
		$limitNews2 = News::find()->where(['is_deleted'=>0])->limit('2')->orderBy(['id' => SORT_DESC])->offset('1')->all();
        return $this->render('index',[
			'limitEvent'=>$limitEvent,
			'limitNews1'=>$limitNews1,
			'limitNews2'=>$limitNews2,
		]);
    }
	
	public function actionBantuan()
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = new HubungiKami();
		$model->created_at=date("Y-m-d H:i:s");
		if ($model->load(Yii::$app->request->post())){
			$model->save();
			Yii::$app->session->setFlash('success', 'Terima Kasih Sudah Mengirim Pesan.');
            return $this->redirect(['bantuan']);
        }
		
        return $this->render('bantuan',[
			'model'=>$model,
		]);
    }

    public function actionAcara()
    {
        return $this->render('acara');
    }

    public function actionBerita()
    {
        return $this->render('berita');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		//$this->layout = 'main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/account']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->redirect(['/account']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionGetLoadMoreEvent($row,$user_id)
	{
		$data=Event::find()->where(['user_id'=>$user_id])->limit($row)->all();
		
		
        return $this->renderAjax('get-load-more-event', [
            'data' => $data,
			'row'=>$row,
        ]);
	}

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
		date_default_timezone_set("Asia/Jakarta");
        $model = new User();
		$model->status = 1;
		$model->role_id = Role::VOLUNTEER;
		$model->is_deleted = 0;
		$model->created_at=date("Y-m-d H:i:s");
		$model->foto='default_user.png';
        if ($model->load(Yii::$app->request->post())) {
			if($_FILES['foto']['name']!=''){				
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}
			
			
			$user=$_POST['User'];
			if($user['email']=='' || $user['username']=='' || $user['password']=='' || $user['nama_lengkap']=='' || $user['no_identitas']=='' || $user['tempat_lahir']=='' || $user['tanggal_lahir']=='' || $user['jenis_kelamin']=='' || $user['no_telp']=='' || $user['alamat_lengkap']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{
				$model->password=md5($model->password);
				$model->save();
				return $this->redirect(['login']);
				Yii::$app->session->setFlash('success', 'Pendaftaran Berhasil.');
			}
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
