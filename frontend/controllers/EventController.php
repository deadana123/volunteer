<?php

namespace frontend\controllers;

use Yii;
use common\models\Event;
use common\models\Log;
use common\models\EventVolunteer;
use common\models\search\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$data=Event::find()->where(['is_deleted'=>0,'status'=>1])->limit('6')->orderBy(['id' => SORT_DESC])->all();
		$allEvent=Event::find()->where(['is_deleted'=>0,'status'=>1])->orderBy(['id' => SORT_DESC])->count();
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'data'=>$data,
			'allEvent'=>$allEvent
        ]);
    }
	
	public function actionJoin($event_id)
    {
		if (!Yii::$app->user->isGuest) {			
			date_default_timezone_set("Asia/Jakarta");
			$model = new EventVolunteer();
			$model->event_id = $event_id;
			$model->user_id = Yii::$app->user->id;
			$model->status=0;
			$model->created_at = date("Y-m-d H:i:s");
			$model->save();
			
			return $this->redirect(['/account/all-event']);
		}else{
			return $this->redirect(['/site/login']);
			Yii::$app->session->setFlash('waning', 'Silahkan Login terlebih dahulu sebagai Volunteer untuk dapat bergabung pada acara.');
		}
    }
	

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionDetail($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$log = new Log();
        $log->created_at = date("Y-m-d H:i:s");
        $log->event_id = $id;
        $log->ip_address = $_SERVER['REMOTE_ADDR'];
        $log->save();

        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionSearch()
    {
		if(isset($_GET['word'])){
			$word=$_GET['word'];
		}else{
			$word='a';
		}
		if($word!=''){			
			$model=Event::find()->where(['LIKE', 'nama', $word])->andWhere(['is_deleted'=>0,'status'=>1])->limit('6')->orderBy(['id' => SORT_DESC])->all();
			$countData=Event::find()->where(['LIKE', 'nama', $word])->andWhere(['is_deleted'=>0,'status'=>1])->orderBy(['id' => SORT_DESC])->count();
		}else{			
			$model=Event::find()->where(['is_deleted'=>0,'status'=>1])->limit('6')->orderBy(['id' => SORT_DESC])->all();
			$countData=Event::find()->count();
		}
        return $this->render('search',[
			'model'=>$model,
			'word'=>$word,
			'countData'=>$countData,
		]);
    }
	
	public function actionGetLoadMoreWithWord($row,$word)
	{
		$data=Event::find()->where(['LIKE', 'nama', $word])->andWhere(['is_deleted'=>0,'status'=>1])->limit($row)->orderBy(['id' => SORT_DESC])->all();
		
		
        return $this->renderAjax('get-load-more-with-word', [
            'data' => $data,
			'row'=>$row,
			'word'=>$word
        ]);
	}
	
	public function actionGetLoadMore($row)
	{
		$data=Event::find()->where(['is_deleted'=>0,'status'=>1])->limit($row)->orderBy(['id' => SORT_DESC])->all();
		
		
        return $this->renderAjax('get-load-more', [
            'data' => $data,
			'row'=>$row,
        ]);
	}

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
