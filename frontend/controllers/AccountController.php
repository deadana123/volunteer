<?php

namespace frontend\controllers;

use Yii;
use common\models\Role;
use common\models\User;
use common\models\Event;
use common\models\EventVolunteer;
use common\models\EventFoto;
use common\models\search\UserSearch;
use common\models\search\EventVolunteerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountController implements the CRUD actions for User model.
 */
class AccountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		date_default_timezone_set("Asia/Jakarta");
        if (!Yii::$app->user->isGuest) {
			$model=User::findOne(Yii::$app->user->id);
            $oldFoto = $model->foto;
			$model->updated_at=date("Y-m-d H:i:s");
			if ($model->load(Yii::$app->request->post())) {	
				if($_FILES['foto']['name']!=''){				
					$foto=$_FILES['foto'];
					$arr = explode(".", $foto['name']);
					$extension = end($arr);

					# generate a unique file name
					$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

					# the path to save file
					$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
					move_uploaded_file($foto['tmp_name'], $path);
					
					//resize image
					//$image = new ImageResize($path);
					//$image->resizeToLongSide(683);
					//$image->save($path);
				}else{
					$model->foto = $oldFoto;
				}

				$model->save();
				return $this->redirect(['index']);
			}
			return $this->render('index',[
				'model'=>$model
			]);
        }else{			
			return $this->redirect(['/site/login']);
		}
    }
	
	public function actionGantiPassword()
    {
		date_default_timezone_set("Asia/Jakarta");
        if (!Yii::$app->user->isGuest) {
			$model=User::findOne(Yii::$app->user->id);
			$model->updated_at=date("Y-m-d H:i:s");
			if ($_POST){
				if(md5($_POST['old_password'])==$model->password){
					if($_POST['password']==$_POST['re_password']){
						$model->password=md5($_POST['password']);
						$model->save();
						Yii::$app->session->setFlash('success', 'Password berhasil diganti.');
						return $this->redirect(['index']);
					}else{						
						Yii::$app->session->setFlash('danger', 'Password tidak sama, Silahkan periksa kembali.');
					}
				}else{
					Yii::$app->session->setFlash('danger', 'Password lama salah, Silahkan periksa kembali.');
				}
			}
			
			return $this->render('ganti-password',[
				'model'=>$model
			]);
        }else{			
			return $this->redirect(['/site/login']);
		}
    }
	
	
	public function actionGetLoadMoreEvent($row,$user_id)
	{
		$data=Event::find()->where(['user_id'=>$user_id])->limit($row)->all();
		
		
        return $this->renderAjax('get-load-more-event', [
            'data' => $data,
			'row'=>$row,
        ]);
	}
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAllEvent(){
		$user = \Yii::$app->user->identity;
		if($user->role_id==Role::VOLUNTEER){
            $Eventdiikuti = EventVolunteer::find()->where(['user_id'=>$user->id])->all();
			$ids = array();
			foreach($Eventdiikuti as $event){
				$ids[]=$event->event_id;
			}
			
            $model = Event::find()->where(['id'=>$ids,'is_deleted'=>0])->all();
		}else{
            $model = Event::find()->where(['user_id'=>$user->id,'is_deleted'=>0])->all();
		}
		
		return $this->render('all-event', [
            'model' => $model,
        ]);
	}
    public function actionDetailEvent($id)
    {	
		$model= new EventFoto();
		$model->event_id=$id;
		$user=\Yii::$app->user->identity;
		$searchModel = new EventVolunteerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->where(['event_id'=>$id]);

		if(isset($_FILES['eventfoto'])){			
			if($_FILES['eventfoto']['name']!=''){			
				$foto=$_FILES['eventfoto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
				$model->save();
				return $this->redirect(['detail-event','id'=>$id]);
			}
		}
       

        return $this->render('detail-event', [
            'model' => Event::findOne($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'id'=>$id
        ]);
    }
	
	public function actionTerimaVolunteer($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = EventVolunteer::findOne($id);
		$model->status=1;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
		$updateEvent = Event::findOne($model->event_id);
		$updateEvent->jumlah_volunteer+=1;
		$updateEvent->save();
		
        return $this->redirect(['detail-event','id'=>$model->event_id]);
    }
	
	public function actionTolakVolunteer($id)
    {
		date_default_timezone_set("Asia/Jakarta");
		$model = EventVolunteer::findOne($id);
		$model->status=2;
		$model->waktu_keputusan_admin=date("Y-m-d H:i:s");
		$model->save();
		
        return $this->redirect(['detail-event','id'=>$model->event_id]);
    }
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	
    public function actionCreateEvent()
    {
        date_default_timezone_set("Asia/Jakarta");
        $model = new Event();
		$model->user_id = Yii::$app->user->id;
		$model->status = 0;
		$model->view = 0;
		$model->created_at=date("Y-m-d H:i:s");
		$model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {	
			if($_FILES['foto']['name']!=''){				
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}else{
				$model->foto = 'default-upload.png';
			}

			$event=$_POST['Event'];
			if($event['nama']=='' || $event['isi']=='' || $event['event_kategori_id']=='' || $event['wilayah_kabupaten_id']=='' || $event['alamat_lengkap']=='' || $event['mulai']=='' || $event['waktu_mulai']=='' || $event['selesai']=='' || $event['waktu_selesai']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{				
				$model->save();
				return $this->redirect(['detail-event','id'=>$model->id]);
			}
        }

        return $this->render('create-event', [
            'model' => $model,
        ]);
    }
	
	public function actionUpdateEvent($id)
    {
        date_default_timezone_set("Asia/Jakarta");
        $model = Event::findOne($id);
		$model->updated_at=date("Y-m-d H:i:s");
		$oldFoto=$model->foto;
        if ($model->load(Yii::$app->request->post())) {	
			if($_FILES['foto']['name']!=''){				
				$foto=$_FILES['foto'];
				$arr = explode(".", $foto['name']);
				$extension = end($arr);

				# generate a unique file name
				$model->foto = "featured_" . date("Y_m_d_H_i_s_") . \Yii::$app->security->generateRandomString(4) . "." . $extension;

				# the path to save file
				$path = \Yii::getAlias("@frontend/web/uploads/") . $model->foto;
				move_uploaded_file($foto['tmp_name'], $path);
				
				//resize image
				//$image = new ImageResize($path);
				//$image->resizeToLongSide(683);
				//$image->save($path);
			}else{
				$model->foto = $oldFoto;
			}
			
			
			$event=$_POST['Event'];
			if($event['nama']=='' || $event['isi']=='' || $event['event_kategori_id']=='' || $event['wilayah_kabupaten_id']=='' || $event['alamat_lengkap']=='' || $event['mulai']=='' || $event['waktu_mulai']=='' || $event['selesai']=='' || $event['waktu_selesai']==''){
				\Yii::$app->session->addFlash("danger", "kolom tidak boleh kosong, harap periksa kembali");
			}else{
				$model->save();
				return $this->redirect(['detail-event','id'=>$id]);
			}
        }

        return $this->render('update-event', [
            'model' => $model,
        ]);
    }
	
	public function actionDeleteEvent($id){
		
		$model = Event::findOne($id);
		$model->is_deleted =1;
		$model->save();
		
		return $this->redirect(['all-event']);
	}

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
