
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\User;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$user=\Yii::$app->user->identity;
$this->title = 'Account';
?>
	<div class="row">
		<div class="col-lg-3" style="padding:10px;">
			<a href="<?=Url::to(['create-event'])?>" style="text-decoration:none;">
			<div style="border:2px dashed #000;min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;display:flex;background:#fff;padding:10px 20px;flex-direction:column;justify-content: center;align-items: center;padding:0 15px 0 0;vertical-align:center;">
						<i class="fa fa-plus" style="color:#999;font-size:36px;width:100%;"></i>
			</div>
			</a>
		</div>           
	   <?php foreach($data as $event){?>
		<div class="col-lg-3" style="padding:10px;">
			<a href="<?=Url::to(['detail-event','id'=>$event->id])?>" style="text-decoration:none;">
				<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:250px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
					<div style="flex:1;"></div>
					<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
						<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->nama?></h4>
						<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->mulai))?></h5>
					</div>
				</div>
			</a>
		</div>
		<?php }?>
	</div>