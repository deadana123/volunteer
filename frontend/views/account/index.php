
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\User;
use common\models\Role;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$user=\Yii::$app->user->identity;
$this->title = 'Account';

$jenis_kelamin = [
    "L" => "Laki - laki",
    "P" => "Perempuan",
];

if($user->role_id==Role::VOLUNTEER){
	$label_konten = 'Acara yang diikuti';
	$button_konten=Html::a("<i class='fa fa-search'></i> Cari Acara",['/event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
}else{
	$label_konten = 'Acara yang dibuat';
	$button_konten=Html::a("<i class='fa fa-plus'></i> Tambah",['create-event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
}
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
    var allcount = Number($("#all").val());
    row = row + 4;
	
	if(row <= allcount){
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		$(".load-more").hide();
	}
});


') ?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').css('background-image', 'url(' + e.target.result + ')');
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#foto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index">

    <div class="container" style="padding:20px;">
		<hr/>
		<div class="col-md-6" style="padding:30px 0;">
			<?php $form = ActiveForm::begin([
					'id' => 'Account',
					//'layout' => 'horizontal',
					'enableClientValidation' => true,
					'errorSummaryCssClass' => 'error-summary alert alert-error',
					'options' => ['enctype' => 'multipart/form-data'],
				]
			);
			?>
				
				<div class="form-group has-feedback" style="display:flex;">
					<label for="foto" id="label_foto" style="position:relative;cursor: pointer;box-shadow: 0px 0px 6px #00000029;width:150px;height:150px;border-radius:50%;overflow:hidden;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>);background-position:center;background-size:cover;">
						<?php if($model->foto!=''){?>

						<?php }?>
						<span style="position: absolute;
    width: 150px;
    height: 50px;
    padding-top: 10px;
    bottom: 0;
    font-size: 24px;
    color: #000;
    text-align: center;background:rgba(238,238,238,0.7);"><i class="fas fa-camera"></i></span>
					</label>
					<input type="file" name="foto" id="foto" style="display:none;">
					<div style="flex:1;">
						<?= Html::a('Ganti Password',['ganti-password'], ['class' => 'btn btn-sm btn-success','style'=>'font-size:14px;text-transform:;font-weight:;display:block;position: absolute;bottom: 0;right: 0;']); ?>
					</div>
				</div>
				<hr/>
				<?= $form->field($model, 'nama_lengkap',[
					'options' => ['class' => 'form-group has-feedback'],
					'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
				])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
				
				<?= $form->field($model, 'username',[
					'options' => ['class' => 'form-group has-feedback'],
					'inputTemplate' => "{input}<span class='glyphicon glyphicon-eye-open form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
				])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
				
				<?= $form->field($model, 'email',[
					'options' => ['class' => 'form-group has-feedback'],
					'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
				])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
				
				<?= $form->field($model, 'no_identitas',[
					'options' => ['class' => 'form-group has-feedback'],
					'inputTemplate' => "{input}<span class='glyphicon glyphicon-credit-card form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
				])->textInput(['maxlength' => true,'style'=>'padding-left:40px;'])->label('No Identitas (KTP/SIM/No Identitas lainnya.)') ?>
				
				<?php if($user->role_id==Role::VOLUNTEER){?>
					<div class="row">
						<div class="col-md-6">
							<?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
						</div>
						<div class="col-md-6">
							<?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true,'type'=>'date']) ?>
						</div>
					</div>
					<?= $form->field($model, 'jenis_kelamin')->dropDownList($jenis_kelamin)->label('Jenis Kelamin') ?>
				<?php }?>
				<?= $form->field($model, 'no_telp',[
					'options' => ['class' => 'form-group has-feedback'],
					'inputTemplate' => "{input}<span class='glyphicon glyphicon-phone form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
				])->textInput(['maxlength' => true,'style'=>'padding-left:40px;'])->label('Nomor Telepon') ?>
				<?= $form->field($model, 'alamat_lengkap')->textArea(['maxlength' => true,'rows'=>6]) ?>
				<hr/>
				<?php echo $form->errorSummary($model); ?>
				<div class="row">
					<div class="col-md-12" style="text-align:left;">
						<?= Html::submitButton('Simpan', ['class' => 'btn btn-large btn-danger','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;']); ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>
		</div>
		<div class="col-md-6">
			<div style="padding-left:50px;">
				<h2><?=$label_konten.' '.$button_konten; ?></h2>
				<hr/>
				<div class="row">
					<?php if($user->role_id==Role::VOLUNTEER){?>
						<?php foreach(common\models\EventVolunteer::find()->where(['user_id'=>$user->id])->andWhere(['<>','status','2'])->limit('3')->all() as $event){?>
							<?php if($event->event->is_deleted==0){?>
								<div class="col-lg-12" style="padding:10px;">
									<a href="<?=Url::to(['/event/detail','id'=>$event->event->id])?>" style="text-decoration:none;">
										<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
											<div style="flex:1;"></div>
											<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
												<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->event->nama?></h4>
												<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->event->mulai))?></h5>
											</div>
										</div>
									</a>
								</div>
							<?php }?>
						<?php }?>
					<?php }else{?>
						<?php foreach(common\models\Event::find()->where(['user_id'=>$user->id,'is_deleted'=>0])->limit('3')->all() as $event){?>
						<div class="col-lg-12" style="padding:10px;">
							<a href="<?=Url::to(['detail-event','id'=>$event->id])?>" style="text-decoration:none;">
								<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
									<div style="flex:1;"></div>
									<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
										<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->nama?></h4>
										<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->mulai))?></h5>
									</div>
								</div>
							</a>
						</div>
						<?php }?>
					<?php }?>
				</div>
				<hr/>
				<div class="row">
					<div class="col-md-12" style="text-align:center;">
						<?= Html::a('Lihat lebih banyak',['all-event'],['class' => 'btn btn-large btn-danger','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

