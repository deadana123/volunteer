<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use common\models\EventVolunteer;
use common\models\User;
use common\models\Event;
/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = $model->nama;
//$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').attr('src', e.target.result);
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#eventfoto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<div class="container" style="margin-bottom:30px;">
	<div class="box box-info">
		<div class="box-body">

			<p>
				<?php //Html::a("<i class='fa fa-arrow-left'></i> Kembali", ['index'], ['class' => 'btn btn-default']) ?>
			</p>
			<hr/>
			<div class="col-md-12" style="display:flex;justify-content:center;align-items:center;">
				<img id="foto" style="width:50%;height:300px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>">
			</div>
			<div class="col-md-12">
				<h2>Detail				
				<?= Html::a("<i class='fa fa-trash'></i> Hapus",['delete-event','id'=>$model->id],['class' => 'btn btn-danger pull-right','style'=>'margin:0 5px;font-size:14px;text-transform:uppercase;font-weight:600;text-align:center;',"title" => "Hapus Data",
                        "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?"]); ?>
				<?= Html::a("<i class='fa fa-pen'></i> Ubah",['update-event','id'=>$model->id],['class' => 'btn btn-warning pull-right','style'=>'margin:0 5px;font-size:14px;text-transform:uppercase;font-weight:600;text-align:center;']); ?>
				</h2>
				<hr/>
				<?= DetailView::widget([
					'model' => $model,
					'attributes' => [
						'nama:ntext',
						[
							'label'  => 'Deskripsi',
							'value'  => $model->isi,
							'captionOptions' => ['style' => 'width:200px'],
						],
						[
							'label'  => 'Kategori',
							'value'  => $model->eventKategori->nama
						],
						[
							'label'  => 'Kabupaten, Provinsi',
							'value'  => $model->wilayahKabupaten->ibukota.', '.$model->wilayahKabupaten->wilayahProvinsi->nama
						],
						'alamat_lengkap',
						[
							'label'  => 'Mulai Event',
							'value'  => date('d F Y',strtotime($model->mulai)).' '.date('H:i',strtotime($model->waktu_mulai))
						],
						[
							'label'  => 'Selesai Event',
							'value'  => date('d F Y',strtotime($model->selesai)).' '.date('H:i',strtotime($model->waktu_selesai))
						],
						'view',
						'kuota',
						'status',
					],
				]) ?>
			</div>
			<div class="col-md-12">
				<hr/>
				<h2 style="display:inline;">Foto
					<?= Html::a("<i class='fa fa-plus'></i> Tambah Foto", ['index'], ['class' => 'btn btn-success pull-right']) ?>
				</h2>
				<!-- Modal -->
				<div class="modal fade" id="exampleModal0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<?php $form = ActiveForm::begin([
									'id' => 'Event',
									'layout' => 'horizontal',
									'enableClientValidation' => false,
									'errorSummaryCssClass' => 'error-summary alert alert-error',
									'options' => ['enctype' => 'multipart/form-data'],
								]
							);
							?>
							<div class="modal-header">
								<h3 class="modal-title" id="exampleModalLabel">Tambah Foto
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</h3>
							</div>
							<div class="modal-body">
								<label for="eventfoto" style="cursor: pointer;box-shadow: 0px 0px 6px #00000029;height: 200px;width: 100%;">
									<img id="label_foto" style="display:none;width:100%;height:100%;padding:20px;">
									<img id="label_foto_icon" style="width: 100px;text-align: center;justify-content: center;align-items: center;display: flex;vertical-align: middle;margin: 80px auto 0;" src="<?=Url::to(['/picture-upload.png'])?>">
								</label>
								<input type="file" name="eventfoto" id="eventfoto" style="display:none;">
							</div>
							<div class="modal-footer">
								<?php echo $form->errorSummary($model); ?>
								<?= Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
								<?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
				<hr/>
				
				<div class="col-md-3">
					<div data-toggle="modal" data-target="#exampleModal0" style="border:3px dashed #000;padding:10px;cursor:pointer;text-align:center;max-height:200px;min-height:200px;display: flex;align-items: center;justify-content: center;">
						<h1><i class="fa fa-plus" style="text-align:center;"></i></h1>
					</div>
				</div>
				<?php foreach(common\models\EventFoto::find()->where(['event_id'=>$model->id])->all() as $eventFoto){?>
					<div class="col-md-3">
						<div data-toggle="modal" data-target="#exampleModal<?=$eventFoto->id?>" style="max-height:200px;min-height:200px;box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);border-radius:20px;">
							<img id="foto" style="width:100%;height:200px;border-radius:20px;" src="<?=Yii::getAlias("@frontend_url/uploads/".$eventFoto->foto);?>">
							
						</div>
					</div>
					<div class="modal fade" id="exampleModal<?=$eventFoto->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h3 class="modal-title" id="exampleModalLabel">Preview
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</h3>
								</div>
								<div class="modal-body">
									<label style="cursor: pointer;box-shadow: 0px 0px 6px #00000029;width: 100%;">
										<img style="width:100%;height:100%;padding:20px;text-align: center;justify-content: center;align-items: center;display: flex;vertical-align: middle;" src="<?=Yii::getAlias("@frontend_url/uploads/".$eventFoto->foto);?>">
									</label>
								</div>
								<div class="modal-footer">
									<?= Html::a("<i class='fa fa-trash'></i> Delete", ["delete-event-foto", "id" => $eventFoto->id,"event_id" => $eventFoto->event_id], [
										"class" => "btn btn-danger",
										"title" => "Hapus Data",
										"data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
										//"data-method" => "GET"
									]) ?>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<div class="col-md-12">
				<hr/>
				<h2 style="display:inline;">Daftar Volunteer</h2>
				<hr/>
				<?php if($model->kuota<=$model->jumlah_volunteer){?>
				<div class="box box-danger" style="border-radius:10px;">
					<div class="box-header" style="border-radius:10px;background:red; padding:10px;color:white;">
						<h3>Kuota Volunteer Telah mencapai limit</h3>
					</div>
				</div>
				<hr/>
				<?php }?>
				<div class="box box-info">
					<div class="box-body">
						<?php \yii\widgets\Pjax::begin(['id' => 'pjax-main', 'enableReplaceState' => false, 'linkSelector' => '#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>

						<?= GridView::widget([
							'layout' => '{summary}{pager}{items}{pager}',
							'dataProvider' => $dataProvider,
							'pager' => [
								'class' => yii\widgets\LinkPager::className(),
								'firstPageLabel' => 'First',
								'lastPageLabel' => 'Last'],
							'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
							'headerRowOptions' => ['class' => 'x'],
							'columns' => [
								[
									'class' => 'yii\grid\SerialColumn',
									'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:center;width:50px'],
								],
								[
									'class' => yii\grid\DataColumn::className(),
									'attribute' => 'Volunteer',
									'label'=>'Volunteer',
									'value' => function ($model) {
										return Html::button($model->user->nama_lengkap,["class" => "btn btn-default btn-change-aktif", "title" => "menunggu",'data-toggle'=>"modal",'data-target'=>"#exampleModalUser$model->user_id"]);
									},
									'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:left;'],
									'format' => 'raw',
								],
								[
									'class' => yii\grid\DataColumn::className(),
									'attribute' => 'Status',
									'label'=>'Status',
									'value' => function ($model) {
										if($model->status==0){								
											return Html::button("Menunggu Konfirmasi",["class" => "btn btn-default btn-change-aktif", "title" => "menunggu"]);
										}else if($model->status==1){								
											return Html::button("Permintaan Bergabung Diterima",["class" => "btn btn-success btn-change-nonaktif", "title" => "diterima"]);
										}else{
											return Html::button("Permintaan Bergabung Ditolak",["class" => "btn btn-danger btn-change-nonaktif", "title" => "ditolak"]);
										}
									},
									'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
									'format' => 'raw',
								],
								[
									'class' => yii\grid\DataColumn::className(),
									'attribute' => 'Action',
									'label'=>'action',
									'value' => function ($model) {
										if($model->status==0){								
											$event=Event::findOne($model->event_id);
											if($event->kuota<=$event->jumlah_volunteer){								
												return Html::button("<i class='fa fa-check'></i>",["class" => "btn btn-success btn-change-aktif", "title" => "Tidak aktif",'disabled'=>true]).' '.Html::button("<i class='fa fa-times'></i>",["class" => "btn btn-danger btn-change-aktif", "title" => "Tidak aktif"]);
											}else{
												return Html::a("<i class='fa fa-check'></i>",['terima-volunteer','id'=>$model->id],["class" => "btn btn-success btn-change-aktif", "title" => "Tidak aktif"]).' '.Html::a("<i class='fa fa-times'></i>",['tolak-volunteer','id'=>$model->id],["class" => "btn btn-danger btn-change-aktif", "title" => "Tidak aktif"]);
											}
										}else{
											return '';
										}
									},
									'contentOptions' => ['nowrap' => 'nowrap', 'style' => 'text-align:right;width:80px'],
									'format' => 'raw',
								],
							],
						]); ?>
						<?php \yii\widgets\Pjax::end() ?>
					</div>
					<?php foreach(EventVolunteer::find()->where(['event_id'=>$id])->all() as $eventVolunteer){?>
						<div class="modal fade" id="exampleModalUser<?=$eventVolunteer->user_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h3 class="modal-title" id="exampleModalLabel">Detail Volunteer
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</h3>
									</div>
									<div class="modal-body">
										<?php $user=User::findOne($eventVolunteer->user_id);?>
										<div class="form-group has-feedback" style="display:flex;">
											<label for="foto" id="label_foto" style="position:relative;cursor: pointer;box-shadow: 0px 0px 6px #00000029;width:100px;height:100px;border-radius:50%;overflow:hidden;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$user->foto);?>);background-position:center;background-size:cover;">
											</label>												
										</div>
										<?= DetailView::widget([
											'model' => $user,
											'attributes' => [
												[
													'label'  => 'Nama Lengkap',
													'value'  => $user->nama_lengkap,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Email',
													'value'  => $user->email,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'No Identitas',
													'value'  => $user->no_identitas,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Tempat Lahir',
													'value'  => $user->tempat_lahir,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Tanggal Lahir',
													'value'  => date("d F Y",strtotime($user->tanggal_lahir)),
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Jenis Kelamin',
													'value'  => $user->jenis_kelamin,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Nomor Telepon',
													'value'  => $user->no_telp,
													'captionOptions' => ['style' => 'width:200px'],
												],
												[
													'label'  => 'Alamat Lengkap',
													'value'  => $user->alamat_lengkap,
													'captionOptions' => ['style' => 'width:200px'],
												],
											],
										]) ?>
									</div>
								</div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
		<hr/>
	</div>
</div>