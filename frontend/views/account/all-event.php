
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\User;
use common\models\Role;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$user=\Yii::$app->user->identity;
$this->title = 'Account';

$jenis_kelamin = [
    "L" => "Laki - laki",
    "P" => "Perempuan",
];

if($user->role_id==Role::VOLUNTEER){
	$label_konten = 'Acara yang diikuti';
	$button_konten=Html::a("<i class='fa fa-search'></i> Cari Acara",['/event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
	
	$link_konten = '/event/detail';
}else{
	$label_konten = 'Acara yang dibuat';
	$button_konten=Html::a("<i class='fa fa-plus'></i> Tambah",['create-event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
	$link_konten = 'detail-event';
}
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
    var allcount = Number($("#all").val());
    row = row + 4;
	
	if(row <= allcount){
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		$(".load-more").hide();
	}
});


') ?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').css('background-image', 'url(' + e.target.result + ')');
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#foto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index">

    <div class="container" style="padding:20px;">
		<hr/>
		<div class="col-md-12">
			<h2><?=$label_konten.' '.$button_konten?></h2>
			<hr/>
			<?php if($user->role_id==Role::VOLUNTEER){?>
				<div class="row">
				   <?php foreach($model as $event){?>
					<div class="col-lg-4" style="padding:10px;">
						<a href="<?=Url::to([$link_konten,'id'=>$event->id])?>" style="text-decoration:none;">
							<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
								<div style="flex:1;padding:15px;text-align:right;">
									<?php 
										$eventVolunteer = common\models\EventVolunteer::find()->where(['event_id'=>$event->id,'user_id'=>$user->id])->one();
										$status_pengajuan = $eventVolunteer->status;
									?>
									
									<?php if($status_pengajuan==0){?>
										<button class="btn btn-sm btn-default">Menunggu konfirmasi</button>
									<?php }elseif($status_pengajuan==1){?>
										<button class="btn btn-sm btn-success">Pengajuan diterima</button>
									<?php }else{?>
										<button class="btn btn-sm btn-danger">Pengajuan ditolak</button>
									<?php }?>
								</div>
								<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
									<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->nama?></h4>
									<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->mulai))?></h5>
								</div>
							</div>
						</a>
					</div>
					<?php }?>
				</div>
			<?php }else{?>
				<div class="row">
				   <?php foreach($model as $event){?>
					<div class="col-lg-4" style="padding:10px;">
						<a href="<?=Url::to([$link_konten,'id'=>$event->id])?>" style="text-decoration:none;">
							<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
								<div style="flex:1;padding:15px;text-align:right;">
									<?php 
										$status_pengajuan = $event->status;
									?>
									
									<?php if($status_pengajuan==0){?>
										<button class="btn btn-sm btn-default">Menunggu konfirmasi</button>
									<?php }elseif($status_pengajuan==1){?>
										<button class="btn btn-sm btn-success">Event diterima</button>
									<?php }else{?>
										<button class="btn btn-sm btn-danger">Event ditolak</button>
									<?php }?>
								</div>
								<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
									<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->nama?></h4>
									<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->mulai))?></h5>
								</div>
							</div>
						</a>
					</div>
					<?php }?>
				</div>
			<?php }?>
			<hr/>
		</div>
	</div>
</div>

