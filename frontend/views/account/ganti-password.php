
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\User;
use common\models\Role;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$user=\Yii::$app->user->identity;
$this->title = 'Account';

$jenis_kelamin = [
    "L" => "Laki - laki",
    "P" => "Perempuan",
];

if($user->role_id==Role::VOLUNTEER){
	$label_konten = 'Acara yang diikuti';
	$button_konten=Html::a("<i class='fa fa-search'></i> Cari Acara",['/event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
}else{
	$label_konten = 'Acara yang dibuat';
	$button_konten=Html::a("<i class='fa fa-plus'></i> Tambah",['create-event'],['class' => 'btn btn-danger pull-right','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;text-align:center;']);
}
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
    var allcount = Number($("#all").val());
    row = row + 4;
	
	if(row <= allcount){
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
	$("#output").load("' . Url::to(["get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		$(".load-more").hide();
	}
});


') ?>
<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').css('background-image', 'url(' + e.target.result + ')');
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#foto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index">

    <div class="container" style="padding:20px;">
		<hr/>
		<div class="col-md-6" style="padding:30px 0;">
			<?php $form = ActiveForm::begin([
					'id' => 'Account',
					//'layout' => 'horizontal',
					'enableClientValidation' => true,
					'errorSummaryCssClass' => 'error-summary alert alert-error',
					'options' => ['enctype' => 'multipart/form-data'],
				]
			);
			?>
			
				<hr/>
				<div class="row">
					<div class="form-group col-md-12">
						<label>Password lama</label>
						<input type="password" class="form-control" name="old_password">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label>Password Baru</label>
						<input type="password" class="form-control" name="password">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label>Ulangi Password Baru</label>
						<input type="password" class="form-control" name="re_password">
					</div>
				</div>
				<hr/>
				<?php echo $form->errorSummary($model); ?>
				<div class="row">
					<div class="col-md-12" style="text-align:left;">
						<?= Html::a("<i class='fas fa-arrow-left'></i> Kembali",['index'], ['class' => 'btn btn-sm btn-default pull-right','style'=>'font-size:14px;text-transform:uppercase;font-weight:600;']); ?>
						<?= Html::submitButton('Simpan', ['class' => 'btn btn-sm btn-danger','style'=>'font-size:14px;text-transform:uppercase;font-weight:600;']); ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

