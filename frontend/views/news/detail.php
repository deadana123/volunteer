
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */


$this->title = 'Berita';
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>


<div class="site-index">
    <div class="body-content container">
		<hr style="border-top:1px solid #fff;"/>
		<p style="font-size:36px;font-weight:600;text-transform:uppercase;"><?=$model->nama?></p>
		<div class="row">
			<div class="col-lg-8" style="padding:10px;">
				<div style="">
					<div style="background:#fff;-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);display:flex;margin-bottom:10px;flex-direction:column;">
						<img src="<?=Url::to(['/uploads/'.$model->foto])?>" style="width:100%;">
						<div style="padding:20px;">
							<div style="text-align:right;">
								<button class="btn btn-sm btn-success" style="margin:0 5px;"><i class="fa fa-pen"></i> <?=common\models\User::findOne($model->created_by)->nama_lengkap?></button>
								<button class="btn btn-sm btn-info"><i class="fa fa-calendar"></i> <?=date('d F Y H:i:s',strtotime($model->created_at))?></button>
							</div>
							<h3 style="color:;text-transform:uppercase;font-weight:600;margin:5px 0;">
								<?=$model->nama?>
							</h3>
							<hr/>
							<p style="color:;font-size:15px;margin:0;text-indent:30px;">
								<?=$model->isi;?>
							</p>
						</div>
					</div>
				</div>
            </div>
			<div class="col-lg-4" style="padding:10px;">
				<div style="display:flex;flex-direction:column;">
					<?php foreach(common\models\News::find()->all() as $news){ ?>
						<div style="background-image:url(<?=Url::to(['/uploads/'.$news->foto])?>);background-size:cover;-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);display:flex;margin-bottom:10px;flex-direction:column;justify-content: flex-end;min-height:150px;">
							<div style="padding:10px;background:rgba(0,0,0,0.7);">
								<h5 style="color:#fff;text-transform:uppercase;font-weight:600;margin:5px 0;"><?=$news->nama?></h5>
								<p style="color:#fff;font-size:13px;margin:0;">
									<?php
										if(strlen($news->isi)>50){
											echo substr($news->isi,0,50).' . . .';
										}else{
											echo $news->isi;
										}
									?>
								</p>
								<a href="#" style="text-align:right;color:;font-size:13px;margin:0;display:block;">Selengkapnya</a>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
        </div>

		<hr/>
    </div>
</div>

