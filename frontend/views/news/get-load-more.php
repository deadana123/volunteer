
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$news1=common\models\News::find()->limit('1')->orderBy(['id' => SORT_DESC])->all();
$news2=common\models\News::find()->limit(2)->offset(1)->orderBy(['id' => SORT_DESC])->all();
$this->title = 'Berita';



?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>
		<div class="row">
			<?php foreach($data as $news){ ?>
				<div class="col-md-6 post" style="padding:10px;">
					<div style="padding:20px;background:#fff;-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);display:flex;border-radius:20px;">
						<img src="<?=Url::to(['/uploads/'.$news->foto])?>" style="width:200px;">
						<div style="padding-left:10px;flex: 1;display: flex;flex-direction: column;">
							<h3 style="color:;text-transform:uppercase;font-weight:600;font-size:15px;margin:5px 0;"><?=$news->nama?></h3>
							<p style="color:;font-size:15px;margin:0;flex:1;">
								<?php
									if(strlen($news->isi)>100){
										echo substr($news->isi,0,100).' . . .';
									}else{
										echo $news->isi;
									}
								?>
							</p>
							<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-align:right;color:;font-size:15px;margin:0;display:block;">Selengkapnya</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<input type="hidden" id="row" value="<?=$row?>">
		