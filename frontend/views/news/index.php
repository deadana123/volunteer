
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$news1=common\models\News::find()->limit('1')->orderBy(['id' => SORT_DESC])->all();
$news2=common\models\News::find()->limit(2)->offset(1)->orderBy(['id' => SORT_DESC])->all();
$this->title = 'Berita';
$urlData = Url::to(['news/get-load-more']);
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
    var allcount = Number($("#all").val());
    row = row + 2;
	
	if(row <= allcount){
		$("#row").val(row);
		$("#output").load("' . Url::to(["news/get-load-more"]) . '?row=" + row);
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
		$("#output").load("' . Url::to(["news/get-load-more"]) . '?row=" + row);
		$(".load-more").hide();
	}
});


') ?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>


<div class="site-index" style="background:#F6F6F6;">
    <div class="body-content container">
		<hr style="border-top:1px solid #fff;"/>
		<p style="font-size:36px;font-weight:600;text-transform:uppercase;">News</p>
		<div class="row">
			<div class="col-lg-8" style="padding:10px;">
				<?php foreach($limitNews1 as $news){?>
					<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-decoration:none;">
						<div style="background-image:url(<?=Url::to(['/uploads/'.$news->foto])?>);background-size:cover;min-height:520px;margin-top:20px;display:flex;justify-content:flex-end;flex-direction:column;">
							<div style="padding: 10px 30px 20px;background: rgba(0,0,0,0.5);">
								<h2 style="color:#fff;text-transform:uppercase;font-weight:600;"><?=$news->nama?></h2>

								<p style="color:#fff;font-size:15px;">
									<?php
										if(strlen($news->isi)>200){
											echo substr($news->isi,0,200).' . . .';
										}else{
											echo $news->isi;
										}
									?>
								</p>
							</div>
						</div>
					</a>
				<?php }?>
            </div>
			<div class="col-lg-4" style="padding:10px;">
				<div style="display:flex;flex-direction:column;">
					<?php foreach($limitNews2 as $news){?>
						<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-decoration:none;">
							<div style="background-image:url(<?=Url::to(['/uploads/'.$news->foto])?>);background-size:cover;min-height:250px;margin-top:20px;display:flex;justify-content:flex-end;flex-direction:column;">
								<div style="padding: 20px;background: rgba(0,0,0,0.5);">
									<h4 style="color:#fff;text-transform:uppercase;font-weight:600;margin:5px 0;"><?=$news->nama?></h4>
									<p style="color:#fff;font-size:15px;margin:0;">
										<?php
											if(strlen($news->isi)>50){
												echo substr($news->isi,0,50).' . . .';
											}else{
												echo $news->isi;
											}
										?>
									</p>
								</div>
							</div>
						</a>
					<?php }?>
				</div>
			</div>
        </div>
		<hr/>
		<div id="output">
			<div class="row">
				<?php foreach($allNews as $news){ ?>
					<div class="col-md-6 post" style="padding:10px;">
						<div style="padding:20px;background:#fff;-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);display:flex;border-radius:20px;">
							<img src="<?=Url::to(['/uploads/'.$news->foto])?>" style="width:200px;">
							<div style="padding-left:10px;flex: 1;display: flex;flex-direction: column;">
								<h3 style="color:;text-transform:uppercase;font-weight:600;font-size:15px;margin:5px 0;"><?=$news->nama?></h3>
								<p style="color:;font-size:15px;margin:0;flex:1;">
									<?php
										if(strlen($news->isi)>100){
											echo substr($news->isi,0,100).' . . .';
										}else{
											echo $news->isi;
										}
									?>
								</p>
								<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-align:right;color:;font-size:15px;margin:0;display:block;">Selengkapnya</a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php if($countNews>2){?>
	        <div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
				<button class="btn btn-default load-more" style="background: #EC4C4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;">Read More</button>
			</div>
			<hr/>
		<?php }elseif($countNews==0){?>
			<p style="color:#ccc;font-size:26px;text-align:center;padding:30px 0;">Tidak ada data yang ditemukan</p>
		<?php }elseif($countNews<=2){?>
			<hr/>
		<?php }?>
		<input type="hidden" id="row" value="2">
		<input type="hidden" id="all" value="<?=$countNews?>">
    </div>
</div>


