<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cabang;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="container">
	<hr/>
	<div class="box box-info col-md-6" style="padding:30px 0;">
		<div class="box-body">
			<?php $form = ActiveForm::begin([
					'id' => 'Login',
					//'layout' => 'horizontal',
					'enableClientValidation' => true,
					//'errorSummaryCssClass' => 'error-summary alert alert-error'
				]
			);
			?>
			
			<?= $form->field($model, 'username',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			
			<?= $form->field($model, 'password',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->passwordInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			<hr/>
			<?php // echo $form->errorSummary($model); ?>
			<div class="row">
				<div class="col-md-12">
					<?= Html::submitButton('Login', ['class' => 'btn btn-danger','style'=>'float:left;margin-right:5px;font-size:15px;text-transform:uppercase;font-weight:600;']); ?>
					<p style="padding:8px;">Belum punya akun ? <?= Html::a("Daftar Sekarang",['/site/signup'],["title" => "Daftar"]);?></p>
				</div>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>