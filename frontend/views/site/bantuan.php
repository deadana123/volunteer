
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\Faq;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */


$this->title = 'Bantuan';
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
	}

	#custom-search-input input{
		border: 0;
		box-shadow: none;
	}

	#custom-search-input button{
		margin: 2px 0 0 0;
		background: none;
		box-shadow: none;
		border: 0;
		color: #666666;
		padding: 0 8px 0 10px;
		border-left: solid 1px #ccc;
	}

	#custom-search-input button:hover{
		border: 0;
		box-shadow: none;
		border-left: solid 1px #ccc;
	}

	#custom-search-input .glyphicon-search{
		font-size: 23px;
	}
	
	h4{
		cursor:pointer;
	}
	.expand_caret {
		transform: scale(1.3);
		margin-left: 8px;
		margin-top: -4px;
	}
	h4[aria-expanded='false'] > .expand_caret {
		transform: scale(1.3) rotate(-180deg);
	}
	
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>
	  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index">
	<div class="container">
		<hr style="border-color:#fff;"/>
		<p style="font-size:30px;font-weight:600;text-transform:uppercase;">FAQ</p>
		<div class="row" style="margin:0;">
			<div class="col-lg-12" style="padding:0;">
				<div class="accordion" id="accordionExample">
					<?php
						$i=1;
						foreach(Faq::find()->where(['status'=>0])->all() as $faq){
							
					?>
							<div class="card">
								<div class="card-header" id="heading<?=$faq->id?>">
									<h4 class="mb-0" style="font-weight:bold;" data-toggle="collapse" data-target="#collapse<?=$faq->id?>" aria-expanded="<?=($i==1)?'true':'false'?>" aria-controls="collapse<?=$faq->id?>">
										<?=$faq->pertanyaan?>
										<span class="pull-right expand_caret"><i class="fas fa-chevron-up"></i></span>
									</h4>
								</div>

								<div id="collapse<?=$faq->id?>" class="collapse <?=($i==1)?'show':''?>" aria-labelledby="heading<?=$faq->id?>" data-parent="#accordionExample">
									<hr/>
									<div class="card-body">
										<p style="font-size:17px;">
											<?=$faq->jawaban?>
										</p>
									</div>
								</div>
								<hr/>
							</div>
					<?php 
							$i++;
						} 
					?>
					
				</div>
			</div>
		</div>
		<hr style="border-color:#fff;"/>
		<p style="font-size:30px;font-weight:600;text-transform:uppercase;">Hubungi Kami</p>
		<div class="row" style="margin:0;">
			<div class="box">
				<div class="col-md-offset-3 col-md-6">
					<?php $form = ActiveForm::begin([
							'id' => 'HubungiKami',
							'enableClientValidation' => false,
							'errorSummaryCssClass' => 'error-summary alert alert-error',
						]
					);
					?>
					<?= $form->field($model, 'nama')->textInput(['maxlength' => true])->label('Nama Lengkap') ?>
					<?= $form->field($model, 'no_telp')->textInput(['maxlength' => true])->label('No Telepon') ?>
					<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
					<?= $form->field($model, 'pesan')->textArea(['rows' => 6])->label('Pesan') ?>
					<hr/>
					<?php echo $form->errorSummary($model); ?>
					<div class="row" style="display:flex;justify-content:center;align-items:center;">
						<?= Html::submitButton('Kirim', ['class' => 'btn btn-danger', 'style'=>'display:block;background: #ff4444;color: #fff;padding: 5px 30px;text-transform: uppercase;font-weight: 600;font-size:16px;']); ?>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<hr/>
	</div>
</div>

