
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\User;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$user=\Yii::$app->user->identity;
$this->title = 'Account';
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
    var allcount = Number($("#all").val());
    row = row + 4;
	
	if(row <= allcount){
		$("#row").val(row);
	$("#output").load("' . Url::to(["site/get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
	$("#output").load("' . Url::to(["site/get-load-more-event"]) . '?row=" + row+"&user_id='.$user->id.'");
		$(".load-more").hide();
	}
});


') ?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index">

    <div class="container" style="padding:20px;">
		<div style="min-height:400px;background-repeat: no-repeat;background-size: cover;height:500px;background-position:center;background-image:url(<?=Url::to(['image/compress/2.jpg'])?>);border:1px solid #eee;border-radius:20px;box-shadow:inset 0 0 0 2000px rgba(0, 0,0, 0.7);">
			<h1 style="color:#fff;">tes</h1>
		</div>
	<hr/>
    </div>
    <div class="body-content container">
		<h2>Event Saya</h2>
		<hr/>
		<div id="output">
			<div class="row">
				<div class="col-lg-3" style="padding:10px;">
					<a href="<?=Url::to(['/site/create-event'])?>" style="text-decoration:none;">
					<div style="border:2px dashed #000;min-height:200px;overflow:hidden;margin-top:10px;border-radius:10px;display:flex;background:#fff;padding:10px 20px;flex-direction:column;justify-content: center;align-items: center;padding:0 15px 0 0;vertical-align:center;">
								<i class="fa fa-plus" style="color:#999;font-size:36px;width:100%;"></i>
					</div>
					</a>
				</div>           
			   <?php foreach(common\models\Event::find()->where(['user_id'=>$user->id])->limit('3')->all() as $event){?>
				<div class="col-lg-3" style="padding:10px;">
					<a href="<?=Url::to(['/event/detail','id'=>$event->id])?>" style="text-decoration:none;">
						<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:250px;overflow:hidden;margin-top:10px;border-radius:10px;background-image:url(<?=Url::to(['/uploads/'.$event->foto])?>);background-size:cover;background-position:center;display:flex;flex-direction:column;">
							<div style="flex:1;"></div>
							<div style="display:flex;flex-direction:column;background:#fff;padding:10px;justify-content: flex-end;">
								<h4 style="color:#000;margin:0;font-weight:bold;padding:5px;"><?=$event->nama?></h4>
								<h5 style="color:#999;margin:0;font-weight:bold;padding:0 5px 0;"><?=date("d F Y",strtotime($event->mulai))?></h5>
							</div>
						</div>
					</a>
				</div>
				<?php }?>
			</div>
		</div>
		<?php if(common\models\Event::find()->where(['user_id'=>$user->id])->count()>3){?>
			<div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
				<button class="btn btn-default load-more" style="background: #ff4444;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;">Read More</button>
			</div>
			<hr/>
		<?php }elseif(common\models\Event::find()->where(['user_id'=>$user->id])->count()<=3){?>
			<hr/>
		<?php }?>
		<input type="hidden" id="row" value="4">
		<input type="hidden" id="all" value="<?=count(common\models\Event::find()->where(['user_id'=>$user->id])->all())?>">
    </div>
</div>

