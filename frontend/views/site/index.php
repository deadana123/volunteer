
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\Slider;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */


$this->title = 'Beranda';
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index" style="background:#F6F6F6;">

    <div class="container" style="padding:20px;">
		<div class="owl-carousel owl-theme">
			<?php foreach(Slider::find()->all() as $slider){?>
				<div class="item" style="background-repeat: no-repeat;background-size: cover;height:400px;background-position:center;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$slider->foto);?>);"></div>
			<?php }?>
		</div>
    </div>
	<hr/>
    <div class="body-content container">
		<form action="<?=Url::to(['/event/search'])?>" method="GET">
			<div class="container">
				<div class="row" style="display:flex;align-items:center;justify-content:center;">
					<div class="col-md-8">
						<div id="custom-search-input">
							<div class="input-group col-md-12">
								<span class="input-group-btn">
									<button class="btn btn-info btn-lg" type="button" style="border:none;color:#EC4C4C;">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
								<input name="word" type="text" class="form-control input-lg" placeholder="Cari Acara" style="padding-left:0;margin:5px;" />
								<span class="input-group-btn">
									<button type="submit" class="btn btn-info btn-lg" type="button" style="background:#EC4C4C;color:#fff;padding:10px 30px;margin:5px;border:none;text-transform:uppercase;">
										Search
									</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<hr/>
		<p style="text-align:center;font-size:36px;font-weight:600;text-transform:uppercase;">Acara Terbaru</p>
		<hr/>
        <div class="row">
            <?php foreach($limitEvent as $event){?>
			<div class="col-lg-4" style="padding:10px;">
				<a href="<?=Url::to(['/event/detail','id'=>$event->id])?>" style="text-decoration:none;">
				<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:300px;overflow:hidden;margin-top:10px;border-radius:10px;">
					<img src="<?=Url::to(['/uploads/'.$event->foto])?>" style="width:100%;min-height:200px;max-height:200px;">
					<div style="display:flex;background:#fff;padding:10px 20px;">
						<div style="display:flex;flex-direction:column;justify-content: center;align-items: center;padding:0 15px 0 0;">
							<h4 style="color:#ff4444;margin:0;font-weight:bold;"><?=date("F",strtotime($event->mulai))?></h4>
							<h1 style="margin:0;font-weight:bold;"><?=date("d",strtotime($event->mulai))?></h1>
						</div>
						<div style="display:flex;flex-direction:column;padding:0 15px 0 0;">
							<h4 style="color:#000;margin:0;font-weight:bold;"><?=$event->nama?></h4>
							<h5 style="color:#777;margin:0;font-weight:bold;"><?=date("l",strtotime($event->mulai))?> <?=date("H:i",strtotime($event->waktu_mulai))?></h5>
							<h5 style="color:#777;margin-bottom:0;font-weight:bold;"><?=$event->wilayahKabupaten->ibukota.', '.$event->wilayahKabupaten->wilayahProvinsi->nama?></h5>
							<p style="margin:0;color:#aaa;font-size:12px;">
								<?php
									if(strlen($event->isi)>100){
										echo substr($event->isi,0,100).' . . .';
									}else{
										echo $event->isi;
									}
								?>
							</p>
						</div>
					</div>
				</div>
				</a>
            </div>
            <?php }?>
        </div>
        <div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
			<?= Html::a("Read More", ['/event'], ['class' => 'btn btn-default','style'=>'background: #EC4C4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;']) ?>
        </div>
		<hr/>
		<p style="font-size:36px;font-weight:600;text-transform:uppercase;">News</p>
		<div class="row">
			<div class="col-lg-8" style="padding:10px;">
				<?php foreach($limitNews1 as $news){?>
					<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-decoration:none;">
						<div style="background-image:url(<?=Url::to(['/uploads/'.$news->foto])?>);background-size:cover;min-height:520px;margin-top:20px;display:flex;justify-content:flex-end;flex-direction:column;">
							<div style="padding: 10px 30px 20px;background: rgba(0,0,0,0.5);">
								<h2 style="color:#fff;text-transform:uppercase;font-weight:600;"><?=$news->nama?></h2>

								<p style="color:#fff;font-size:15px;">
									<?php
										if(strlen($news->isi)>100){
											echo substr($news->isi,0,100).' . . .';
										}else{
											echo $news->isi;
										}
									?>
								</p>
							</div>
						</div>
					</a>
				<?php }?>
            </div>
			<div class="col-lg-4" style="padding:10px;">
				<div style="display:flex;flex-direction:column;">
					<?php foreach($limitNews2 as $news){?>
						<a href="<?=Url::to(['/news/detail','id'=>$news->id])?>" style="text-decoration:none;">
							<div style="background-image:url(<?=Url::to(['/uploads/'.$news->foto])?>);background-size:cover;min-height:250px;margin-top:20px;display:flex;justify-content:flex-end;flex-direction:column;">
								<div style="padding: 20px;background: rgba(0,0,0,0.5);">
									<h4 style="color:#fff;text-transform:uppercase;font-weight:600;margin:5px 0;"><?=$news->nama?></h4>
									<p style="color:#fff;font-size:15px;margin:0;">
										<?php
											if(strlen($news->isi)>100){
												echo substr($news->isi,0,100).' . . .';
											}else{
												echo $news->isi;
											}
										?>
									</p>
								</div>
							</div>
						</a>
					<?php }?>
				</div>
			</div>
        </div>
		<div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
			<?= Html::a('Read More', ['/news'], ['class' => 'btn btn-default','style'=>'background: #EC4C4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;']) ?>
        </div>
		<hr/>
    </div>
</div>

