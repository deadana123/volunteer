<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
//$this->params['breadcrumbs'][] = $this->title;

$jenis_kelamin = [
    "L" => "Laki - laki",
    "P" => "Perempuan",
];

?>

<?php
$js = <<<js

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#label_foto').show();
                $('#label_foto').css('background-image', 'url(' + e.target.result + ')');
				$('#label_foto_icon').hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
	
	$("#foto").change(function(){
        readURL(this);
    });
	
js;
$this->registerJs($js);
?>
<div class="container">
	<hr/>
	<div class="box box-info col-md-6" style="padding:30px 0;">
		<div class="box-body">
			<?php $form = ActiveForm::begin([
					'id' => 'Role',
					//'layout' => 'horizontal',
					'enableClientValidation' => true,
					//'errorSummaryCssClass' => 'error-summary alert alert-error',
					'options' => ['enctype' => 'multipart/form-data'],
				]
			);
			?>
			<div class="form-group has-feedback">
				<label for="foto" id="label_foto" style="position:relative;cursor: pointer;box-shadow: 0px 0px 6px #00000029;width:150px;height:150px;border-radius:50%;overflow:hidden;background-image:url(<?=Yii::getAlias("@frontend_url/uploads/".$model->foto);?>);background-position:center;background-size:cover;">
					<?php if($model->foto!=''){?>

					<?php }?>
					<span style="position: absolute;width: 150px;height: 50px;padding-top: 10px;bottom: 0;font-size: 24px;color: #000;text-align: center;background:rgba(238,238,238,0.7);"><i class="fas fa-camera"></i></span>
				</label>
				<input type="file" name="foto" id="foto" style="display:none;">
			</div>
			<?= $form->field($model, 'email',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			<?= $form->field($model, 'username',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-eye-open form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			
			<?= $form->field($model, 'password',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->passwordInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			<?= $form->field($model, 'nama_lengkap',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;']) ?>
			
			<?= $form->field($model, 'no_identitas',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-credit-card form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;'])->label('No Identitas (KTP/SIM/No Identitas lainnya.)') ?>
			
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-md-6">
					<?= $form->field($model, 'tanggal_lahir')->textInput(['maxlength' => true,'type'=>'date']) ?>
				</div>
			</div>
			<?= $form->field($model, 'jenis_kelamin')->dropDownList($jenis_kelamin)->label('Jenis Kelamin') ?>
			<?= $form->field($model, 'no_telp',[
				'options' => ['class' => 'form-group has-feedback'],
				'inputTemplate' => "{input}<span class='glyphicon glyphicon-phone form-control-feedback' style='left:0;border-right:1px solid #eee;'></span>"
			])->textInput(['maxlength' => true,'style'=>'padding-left:40px;'])->label('Nomor Telepon') ?>
			<?= $form->field($model, 'alamat_lengkap')->textArea(['maxlength' => true,'rows'=>6]) ?>
			<hr/>
			<?php // $form->errorSummary($model); ?>
			<div class="row">
				<div class="col-md-12" style="text-align:center;">
					<?= Html::submitButton('Daftar', ['class' => 'btn btn-large btn-danger form-control','style'=>'font-size:16px;text-transform:uppercase;font-weight:600;']); ?>
				</div>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
