
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */

$this->title = 'Acara';
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>
		<div class="row">
			<?php foreach($data as $event){ ?>
				<div class="col-lg-4" style="padding:10px;">
					<a href="<?=Url::to(['/event/detail','id'=>$event->id])?>" style="text-decoration:none;">
						<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
		-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
		box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:482px;overflow:hidden;margin-top:10px;border-radius:10px;">
							<img src="<?=Url::to(['/uploads/'.$event->foto])?>" style="width:100%;min-height:200px;max-height:200px;">
							<div style="display:flex;background:#fff;padding:20px;">
								<div style="display:flex;flex-direction:column;padding:0 15px 0 0;">
									<h3 style="color:#000;margin:0;font-weight:bold;font-size:20px;"><?=$event->nama?></h3>
									<h4 style="color:#777;margin-bottom:5px;font-weight:bold;">Waktu</h4>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-calendar" style="color:#EC4C4C;"></i>&ensp;<?=date("l,F d Y",strtotime($event->mulai))?></p>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-clock" style="color:#EC4C4C;"></i>&ensp;<?=date("H:i",strtotime($event->waktu_mulai))?></p>
									<h4 style="color:#777;margin-bottom:5px;font-weight:bold;">Tempat</h4>
									<?php
										$alamat_lengkap = $event->alamat_lengkap.', '.$event->wilayahKabupaten->ibukota.', '.$event->wilayahKabupaten->wilayahProvinsi->nama;
									?>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-map-marker-alt" style="color:#EC4C4C;"></i>&ensp;
										<?php
											if(strlen($alamat_lengkap)>25){
												echo substr($alamat_lengkap,0,25).' . . .';
											}else{
												echo $alamat_lengkap;
											}
										?>
									</p>
									<p style="margin-top:10px;color:#aaa;font-size:12px;">
										<?php
											if(strlen($event->isi)>200){
												echo substr($event->isi,0,200).' . . .';
											}else{
												echo $event->isi;
											}
										?>
									</p>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
		<input type="hidden" id="row" value="<?=$row?>">
		