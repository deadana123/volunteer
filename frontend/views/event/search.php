
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */


$this->title = 'Acara';
?>
<?php $this->registerJs('

$(".load-more").on("click",function(){
	var row = Number($("#row").val());
	var word = $("#word").val();
    var allcount = Number($("#all").val());
    row = row + 6;
	
	if(row <= allcount){
		$("#row").val(row);
		$("#output").load("' . Url::to(["event/get-load-more-with-word"]) . '?row=" + row+"&word="+word);
		if(row==allcount){
			$(".load-more").hide();
		}
	}else{
		$("#row").val(row);
		$("#output").load("' . Url::to(["event/get-load-more-with-word"]) . '?row=" + row+"&word="+word);
		$(".load-more").hide();
	}
});


') ?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index" style="background:#F6F6F6;">
    <div class="body-content container" style="margin-top:10px;">
		<form action="<?=Url::to(['/event/search'])?>" method="GET">
			<div class="container">
				<div class="row" style="display:flex;align-items:center;justify-content:center;margin-right:0;">
					<p style="font-size:36px;font-weight:600;text-transform:uppercase;flex:2;">Acara</p>
					<div class="col-md-12" style="flex:1;">
						<div id="custom-search-input">
							<div class="input-group col-md-12">
								<span class="input-group-btn">
									<button class="btn btn-info btn-lg" type="button" style="border:none;color:#EC4C4C;">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
								<input type="text" id="word" name="word" value="<?=$word?>" class="form-control input-lg" placeholder="Cari Acara" style="padding: 15px 0 15px;height: 20px;margin:5px;" />
								<span class="input-group-btn">
									<button type="submit" class="btn btn-info btn-lg" type="button" style="background:#EC4C4C;color:#fff;padding:5px 20px;margin:5px;border:none;text-transform:uppercase;font-weight:600;font-size:15px;">
										Search
									</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<hr/>
		<div id="output">
			<div class="row">
				<?php foreach($model as $event){?>
				<div class="col-lg-4" style="padding:10px;">
					<a href="<?=Url::to(['/event/detail','id'=>$event->id])?>" style="text-decoration:none;">
						<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
		-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
		box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:482px;overflow:hidden;margin-top:10px;border-radius:10px;">
							<img src="<?=Url::to(['/uploads/'.$event->foto])?>" style="width:100%;min-height:200px;max-height:200px;">
							<div style="display:flex;background:#fff;padding:20px;">
								<div style="display:flex;flex-direction:column;padding:0 15px 0 0;">
									<h3 style="color:#000;margin:0;font-weight:bold;font-size:20px;"><?=$event->nama?></h3>
									<h4 style="color:#777;margin-bottom:5px;font-weight:bold;">Waktu</h4>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-calendar" style="color:#EC4C4C;"></i>&ensp;<?=date("l,F d Y",strtotime($event->mulai))?></p>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-clock" style="color:#EC4C4C;"></i>&ensp;<?=date("H:i",strtotime($event->waktu_mulai))?></p>
									<h4 style="color:#777;margin-bottom:5px;font-weight:bold;">Tempat</h4>
									<?php
										$alamat_lengkap = $event->alamat_lengkap.', '.$event->wilayahKabupaten->ibukota.', '.$event->wilayahKabupaten->wilayahProvinsi->nama;
									?>
									<p style="color:#777;margin-bottom:0;"><i class="fa fa-map-marker-alt" style="color:#EC4C4C;"></i>&ensp;
										<?php
											if(strlen($alamat_lengkap)>25){
												echo substr($alamat_lengkap,0,25).' . . .';
											}else{
												echo $alamat_lengkap;
											}
										?>
									</p>
									<p style="margin-top:10px;color:#aaa;font-size:12px;">
										<?php
											if(strlen($event->isi)>200){
												echo substr($event->isi,0,200).' . . .';
											}else{
												echo $event->isi;
											}
										?>
									</p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<?php }?>
			</div>
        </div>
		<?php if($countData>2){?>
	        <div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
				<button class="btn btn-default load-more" style="background: #EC4C4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;">Read More</button>
			</div>
			<hr/>
		<?php }elseif($countData==0){?>
			<p style="color:#ccc;font-size:26px;text-align:center;padding:30px 0;">Tidak ada data yang ditemukan</p>
		<?php }elseif($countData<=2){?>
			<hr/>
		<?php }?>
			<input type="hidden" id="row" value="6">
			<input type="hidden" id="all" value="<?=$countData?>">
    </div>
</div>

