
<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use common\models\EventVolunteer;
/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 * @var yii\widgets\ActiveForm $form
 */


$this->title = 'Detail Acara';
?>
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.carousel.css']) ?>" rel="stylesheet" type="text/css">
    <link href="<?= url::to(['OwlCarousel/dist/assets/owl.theme.default.min.css']) ?> " rel="stylesheet" type="text/css">
    <style type="text/css">
      *{
        margin: 0;
        padding: 0;
      }
      .img-fluid{
        width: 100%;
        height:30%;
        display: block;
      }
	  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
	  
    </style>
	<script src="<?= url::to(['OwlCarousel/docs/assets/vendors/jquery.min.js']) ?>" type="text/javascript"></script>
	  <script src="<?= url::to(['OwlCarousel/dist/owl.carousel.js']) ?>" type="text/javascript"></script>

	  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 3000,
                //autoplayHoverPause: true
              });
            })
          </script>

<div class="site-index" style="background:#F6F6F6;">
    <div class="body-content container">
        <div class="row">
			<div class="col-lg-8 col-lg-offset-2" style="padding:10px;">
				<div style="-webkit-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
-moz-box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);
box-shadow: 0px 0px 16px 1px rgba(0,0,0,0.21);min-height:300px;overflow:hidden;margin-top:10px;border-radius:10px;width:100%;">
					<img src="<?=Url::to(['/uploads/'.$model->foto])?>" style="width:100%;">
					<div style="display:flex;background:#fff;padding:20px 100px;">
						<div style="display:flex;flex-direction:column;padding:0 15px 0 0;width:100%;">
							<p style="font-size:30px;font-weight:600;color:#000;margin-bottom:0;text-transform:uppercase;text-align:center;"><?=$model->nama?></p>
							<hr style="border:1px solid #EC4C4C"/>
							<p style="font-size:26px;color:#000;margin-bottom:10px;">Detail</p>
							<p style="font-size:16px;color:#777;margin-bottom:0;"><i class="fa fa-map-marker-alt" style="color:#EC4C4C;"></i>&ensp;<?=$model->alamat_lengkap.', '.$model->wilayahKabupaten->ibukota.', '.$model->wilayahKabupaten->wilayahProvinsi->nama?></p>
							<p style="font-size:16px;color:#777;margin-bottom:0;">
								<i class="fa fa-calendar" style="color:#EC4C4C;"></i>&ensp;<?=date("d F Y",strtotime($model->mulai)).' - '.date("d F Y",strtotime($model->selesai))?>
							</p>
							<p style="font-size:16px;color:#777;margin-bottom:0;"><i class="fa fa-clock" style="color:#EC4C4C;"></i>&ensp;<?='Open at '.date("H:i",strtotime($model->waktu_mulai))?></p>
							<hr/>
							<p style="font-size:26px;color:#000;margin-bottom:0;">Deskripsi</p>
							<p style="margin-top:10px;color:#aaa;font-size:15px;text-indent:40px;"><?=$model->isi?></p>
							<hr/>
							<div class="row" style="padding-top:20px;display:flex;align-items:center;justify-content:center;">
								<?php
									if (!Yii::$app->user->isGuest) {
										$user_id = Yii::$app->user->id;
										$eventVolunteer = EventVolunteer::find()->where(['user_id'=>$user_id,'event_id'=>$model->id])->one();
										
										if($eventVolunteer){											
											if($eventVolunteer->status==0){
												echo Html::button("Menunggu konfirmasi",['class' => 'btn btn-default pull-right','style'=>'padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;']);
											}elseif($eventVolunteer->status==1){
												echo Html::button("Pengajuan diterima",['class' => 'btn btn-success pull-right','style'=>'color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;']);
											}else{
												echo Html::button("Pengajuan ditolak",['class' => 'btn btn-danger pull-right','style'=>'background: #EC4C4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;']);
											}
										}else{
											if($model->kuota<=$model->jumlah_volunteer){
												echo Html::button("Kuota Volunteer Telah mencapai limit",['class' => 'btn btn-danger pull-right','style'=>'color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;',"title" => "limit",'disabled'=>true]);
											}else{
												echo Html::a("Gabung Sekarang",['join','event_id'=>$model->id],['class' => 'btn btn-danger pull-right','style'=>'background: #008D4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;',"title" => "Join","data-confirm" => "Apakah Anda yakin ingin menjadi volunteer event ini ?"]);
											}
										}
									}else{
										$display='block';
										if($model->kuota<=$model->jumlah_volunteer){
											echo Html::button("Kuota Volunteer Telah mencapai limit",['class' => 'btn btn-danger pull-right','style'=>'color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;',"title" => "limit",'disabled'=>true]);
										}else{
											echo Html::a("Gabung Sekarang",['join','event_id'=>$model->id],['class' => 'btn btn-danger pull-right','style'=>'background: #008D4C;color: #fff;padding: 10px 30px;text-transform: uppercase;font-weight: 600;font-size:18px;',"title" => "Join","data-confirm" => "Apakah Anda yakin ingin menjadi volunteer event ini ?"]);
										}
									}
								?>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		<hr/>
    </div>
</div>

