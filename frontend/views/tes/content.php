<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<style>
	.col-md-8{
		width: 66.66666667%;
	}
	.col-md-4 {
		width: 33.33333333%;
	}
	.col-md-8,
	.col-md-4 {
		float:left;
	}
</style>
<div class="content-wrapper" style="overflow:auto;margin-top:0;">
    <section class="content-header">
        <!--
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>
        -->
        <?php
        /*
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        )
        */
        ?>
    </section>

    <section class="content">
        <?php // Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019 <a href="joca.id">Joca.id</a></strong> All rights
    reserved.
</footer>