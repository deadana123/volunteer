<?php

use common\models\User;
use common\models\RoleMenu;
use common\components\SidebarMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var User $user */
$user = Yii::$app->user->identity;
?>
<style>
	#header{
		-webkit-box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
		-moz-box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
		box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
	}
	
    #header-menu-nav{
		flex:1;
		text-align:right;
	}
	
    #header-menu-nav > ul{
		margin-top:14px;
	}
	
    #header-menu-nav > ul > li{
		display:inline-block;
    }
	
	@media only screen and (min-width:1200px) {
		#header-menu-nav > ul > li{
			margin-left:20px;
		}
	}
	
    #header-menu-nav > ul > li > a.active:before,
    #header-menu-nav > ul > li > a:hover:before{
		//transition: all 0.3s ease-in-out 0s;
		//border-bottom:2px solid #fff;
		width:100%;
		visibility:visible;
		left:0;
	}
	
    #header-menu-nav > ul > li > a:before{
		content: "";
		position: absolute;
		width: 0;
		height: 2px;
		bottom: 0;
		left:50%;
		background-color: #fff;
		visibility: hidden;
		transition: all 0.3s ease-in-out 0s;
	}
	
    #header-menu-nav > ul > li > a{
		font-family:'Product_Sans',sans-serif;
		color:#fff;
		padding:20px 20px 10px;
		font-size:15px;
		position:relative;
	}
	.content-wrapper{
		margin-top: 75px;
	}
	.content-wrapper,.main-footer{
		margin-left: 260px;
		background:#fff;
	}
</style>
<header id="header" class="container-fluid" style="z-index:2;position:relative;width:100%;padding-top:0;background:red;padding-left:0;padding-right:0;">
	<div class="container register_container-fluid" style="min-width: 1024px;padding-top: 15px;background:#ff0000;padding-bottom:15px;">
		<div style="display:flex;">
			<img id='register_logo' style="width:150px;height:100%;"src="<?=Url::to(['/images/joca_logo_putih_crop.png'])?>">
			<nav id="header-menu-nav">
				<ul>
					<?php foreach(common\models\Menu::find()->where(['parent_id' => null])->all() as $m){?>
						<?php 
							$hasAccess=RoleMenu::find()->where(["menu_id" => $m->id, "role_id" => Yii::$app->user->identity->role_id])->one();
							if($hasAccess){
								if($m->controller==''){
									$controller = strtolower(str_replace(' ', '-', $m->name));
								}else{
									$controller=$m->controller;									
								}
								/* Check Parent or Child is active*/
								$checkMenu=common\models\Menu::find()->where(['controller' => Yii::$app->controller->id])->one();
								if($m->controller==Yii::$app->controller->id){
									$active='active';									
								}else{
									if($checkMenu->parent_id==NULL){
										$active='';
									}elseif($checkMenu->parent_id!=NULL){
										$getParent = common\models\Menu::findOne($checkMenu->parent_id);
										if($getParent->id==$m->id){
											$active='active';
										}else{										
											$active='';
										}
									}
								}
						?>
								<li><a href="<?=Url::to(['/'.$controller])?>" class="<?=($active=='active')?'active':'';?>"><?=$m->name?></a></li>
						<?php 
							}
						?>
					<?php }?>
						<li><?= Html::a('Logout',['/site/logout'],['data-method' => 'post']) ?></li>
				</ul>
			</nav>
		</div>
	</div>					
</header>