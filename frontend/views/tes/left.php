<?php
use common\components\SidebarMenu;
use common\models\User;
use dmstr\widgets\Menu;

/** @var User $user */
$user = Yii::$app->user->identity;
?>
<style>
    .sidebar-menu .svg-inline--fa {
        /*width: 25px !important;*/
    }
	
	.sidebar-menu{
		overflow:visible !important;
		
	}
	
	.sidebar-menu>li.active>a{
		color:#FF0000 !important;
		background:#fff !important;
		border:none;
		font-family:'Product_Sans',sans-serif;
		margin-left:3px;
	}
	
	.sidebar-menu>li.active>a :after{
		content:'';
		width:5px;
		height:100%;
		position:absolute;
		background:#fff;
		right:0;
		top:0;
	}
	
	.sidebar-menu>li>a{
		font-weight:bold;
		color:#8A909D !important;
		background:none !important;
		font-family:'Product_Sans',sans-serif;
		padding-bottom:0;
	}
	
	.sidebar-menu>li>a>svg{
		display:none;
	}
	
	@media (max-width: 767px){
		.main-sidebar {
			transform:translate(0, 0);;
		}
	}
</style>
<aside class="main-sidebar" style="border-right:1px solid #ddd;background:#fff;width:260px;padding-top:100px;z-index:1;">

    <section class="sidebar">

        <?php
		$getParentId=common\models\Menu::find()->where(['controller'=>Yii::$app->controller->id])->one();
		if($getParentId->parent_id==NULL){			
			$items = SidebarMenu::getMenu(Yii::$app->user->identity->role_id,Yii::$app->controller->id);
		}else{
			$items = SidebarMenu::getMenu(Yii::$app->user->identity->role_id,$getParentId->parent_id);
		}
        Menu::$iconClassPrefix = '';
        echo Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => $items["menus"]
            ]
        ) ?>

    </section>

</aside>
