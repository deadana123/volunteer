<?php

use common\models\User;
use common\models\RoleMenu;
use common\components\SidebarMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var User $user */
$user = Yii::$app->user->identity;
?>
<footer class="container-fluid" style="border-top:5px solid #ff4444;background:#0D0F10;z-index:2;position:relative;width:100%;padding-top:0;padding-left:0;padding-right:0;">
	<div class="container register_container-fluid" style="min-width: 1024px;background:;">
		<div style="display:flex;">
			<img id='register_logo' style="width:150px;height:100%;"src="<?=Url::to(['/image/volunteerku-logo.png'])?>">
			<p style="padding:30px 30px 0;color:#fff;font-size:16px;">Volunteerku merupakan salah satu aplikasi penyedia jasa volunteer di bidang website. 
			Aplikasi ini dihadirkan untuk masyarakat yang ingin bergabung menjadi relawan serta pemilik perusahaan ataupun oragnisasi atau komunitas dapat juga membuat acara yang membutuhkan relawan di aplikasi Volunteerku 
			Dengan tampilan yang minimalis dan sederhana, Volunteerku diharapkan mampu diakses dengan mudah. Kedepannya kami ingin terus mengembangkan aplikasi ini untuk membantu mempermudah masyrakat dalam mengaksesnya.</p>
		</div>
		<hr style="border-top: 1px solid #0D0F10;"/>
		<div class="row">
			<div class="col-lg-5">
				<h2 style="color:#fff;">Volunteerku</h2>
				<ul>
					<li style="list-style:none;padding:5px 0;"><a href="#" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">Tentang Kami</a></li>
					<li style="list-style:none;padding:5px 0;"><a href="<?=Url::to(['/event'])?>" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">Acara</a></li>
					<li style="list-style:none;padding:5px 0;"><a href="<?=Url::to(['/news'])?>" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">Berita</a></li>
					<li style="list-style:none;padding:5px 0;"><a href="<?=Url::to(['/site/bantuan'])?>" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">Bantuan</a></li>
				</ul>
			</div>
			<div class="col-lg-3">
				<div class="background:red;">
					<h2 style="color:#fff;">Hubungi Kami</h2>
					<ul>
						<li style="list-style:none;padding:5px 0;">
							<a href="#" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">
								<i class="fa fa-phone" style="-ms-transform: rotateY(180deg);transform: rotateY(180deg);"></i> 0123456789
							</a>
						</li>
						<li style="list-style:none;padding:5px 0;">
							<a href="#" style="text-decoration:none;fontfont-family:'Product_Sans',sans-serif;color:#999;font-size:15px;">
							<i class="fa fa-envelope"></i> Acara</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="background:red;">
					<h2 style="text-align:center;color:#fff;">Ikuti Kami</h2>
					<div style="display:flex;justify-content:center;">
						<img src="<?=Url::to(['image/instagram.png'])?>" style="width:50px;background:white;border-radius:50%;border:2px solid #fff;">&ensp;
						<img src="<?=Url::to(['image/facebook.png'])?>" style="width:50px;background:white;border-radius:50%;border:2px solid #fff;">
						&ensp;
						<img src="<?=Url::to(['image/twitter.png'])?>" style="width:50px;background:white;border-radius:50%;border:2px solid #fff;">
					</div>
				</div>
			</div>
		</div>
		<p style="text-align:center;color:#fff;">&copy; Volunteerku 2020</p>
	</div>					
</footer>