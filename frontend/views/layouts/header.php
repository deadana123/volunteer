<?php

use common\models\User;
use common\models\RoleMenu;
use common\components\SidebarMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var User $user */
$user = Yii::$app->user->identity;
?>
<style>
	#header{
		-webkit-box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
		-moz-box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
		box-shadow: -2px 0px 10px 0px rgba(0,0,0,0.72);
	}
	
    #header-menu-nav{
		flex:1;
		text-align:right;
	}
	
    #header-menu-nav > ul{
		margin-top:14px;
	}
	
    #header-menu-nav > ul > li{
		display:inline-block;
    }
	
	@media only screen and (min-width:1200px) {
		#header-menu-nav > ul > li{
			margin-left:20px;
		}
	}
	
    #header-menu-nav > ul > li > a.active:before,
    #header-menu-nav > ul > li > a:hover:before{
		//transition: all 0.3s ease-in-out 0s;
		//border-bottom:2px solid #fff;
		width:100%;
		visibility:visible;
		left:0;
	}
	
    #header-menu-nav > ul > li > a:before{
		content: "";
		position: absolute;
		width: 0;
		height: 2px;
		bottom: 0;
		left:50%;
		background-color: #EC4C4C;
		visibility: hidden;
		transition: all 0.3s ease-in-out 0s;
	}
	
    #header-menu-nav > ul > li > a{
		font-family:'Product_Sans',sans-serif;
		color:#999;
		padding:20px 20px 10px;
		font-size:15px;
		position:relative;
		text-decoration:none;
	}
	.content-wrapper{
		margin-top: 75px;
	}
	.content-wrapper,.main-footer{
		margin-left: 260px;
		background:#999;
	}
</style>
<header id="header" class="container-fluid" style="z-index:2;position:relative;width:100%;padding-top:0;padding-left:0;padding-right:0;">
	<div class="container register_container-fluid" style="min-width: 1024px;background:;">
		<div style="display:flex;">
			<a href="<?=Url::to(['/'])?>">
				<img id='register_logo' style="width:70px;height:100%;"src="<?=Url::to(['/image/volunteerku-logo.png'])?>">
			</a>
			<nav id="header-menu-nav" style="padding-top:10px;">
				<ul>
					<li>
						<a href="<?=Url::to(['/event'])?>" style="color:#2A2A2A;font-weight:700;">Acara</a>
						<a href="<?=Url::to(['/news'])?>" style="color:#2A2A2A;font-weight:700;">Berita</a>
						<a href="<?=Url::to(['/site/bantuan'])?>" style="color:#2A2A2A;font-weight:700;">Bantuan</a>					
						<?php if (!Yii::$app->user->isGuest){?>
							<a href="<?=Url::to(['/account'])?>" style="color:#2A2A2A;font-weight:700;">Account</a>
							<?= Html::a('Logout',['/site/logout'],['data-method' => 'post', 'style' => 'color:#2A2A2A;font-weight:700;']) ?>
						<?php }else{ ?>
							<a href="<?=Url::to(['/site/login'])?>" style="color:#2A2A2A;font-weight:700;">Login</a>
						<?php }?>
					</li>
				</ul>
			</nav>
		</div>
	</div>					
</header>