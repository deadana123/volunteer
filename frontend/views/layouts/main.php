<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<style>
		@font-face {
			font-family: 'Product_Sans';
			font-style: normal;
			font-weight: normal;
			src: url('<?= Url::to(["font/ProductSansRegular.eot"]) ?>');
			src: url('<?= Url::to(["font/ProductSansRegular.woff"]) ?>') format("woff");
			src: url('<?= Url::to(["font/ProductSansRegular.woff2"]) ?>') format("woff2");
			src: url('<?= Url::to(["font/ProductSansRegular.otf"]) ?>') format("opentype");
			src: url('<?= Url::to(["font/ProductSansRegular.svg#ProductSansRegular"]) ?>') format("svg");
		  }
	</style>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap" style="padding:0;">
    <?php
    // NavBar::begin([
    //     'brandLabel' => Yii::$app->name,
    //     'brandUrl' => Yii::$app->homeUrl,
    //     'options' => [
    //         'class' => 'navbar-inverse navbar-fixed-top',
    //     ],
    // ]);
    // $menuItems = [
    //     ['label' => 'Beranda', 'url' => ['/index/index']],
    //     ['label' => 'About', 'url' => ['/site/about']],
    //     ['label' => 'Contact', 'url' => ['/site/contact']],
    // ];
    // if (Yii::$app->user->isGuest) {
    //     $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    //     $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    // } else {
    //     $menuItems[] = '<li>'
    //         . Html::beginForm(['/site/logout'], 'post')
    //         . Html::submitButton(
    //             'Logout (' . Yii::$app->user->identity->username . ')',
    //             ['class' => 'btn btn-link logout']
    //         )
    //         . Html::endForm()
    //         . '</li>';
    // }
    // echo Nav::widget([
    //     'options' => ['class' => 'navbar-nav navbar-right'],
    //     'items' => $menuItems,
    // ]);
    // NavBar::end();
    
    ?>
    <!--
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container">

		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="index">
		  <img src="image/volunteerku-logo.png" style=" width: 70px; margin-top:-25px"></img>
		  </a>
		</div>


		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="<?= url::to(['site/acara']) ?>">Acara</a></li>
			<li><a href="<?= url::to(['site/berita']) ?>">Berita</a></li>
			<li><a href="#">Bantuan</a></li>
			<li><a href="<?= url::to(['site/login']) ?>">Login</a></li>
		  </ul>
		</div>
	  </div>
	</nav>
	-->
	<?= $this->render(
		'header.php'
	) ?>
    <div class="container-fluid" style="padding:0;">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

	<?= $this->render(
		'footer.php'
	) ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
